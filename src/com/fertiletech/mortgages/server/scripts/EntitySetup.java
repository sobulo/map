/**
 * 
 */
package com.fertiletech.mortgages.server.scripts;


import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.mortgages.server.entities.ApplicationParameters;
import com.fertiletech.mortgages.server.entities.EntityDAO;
import com.fertiletech.mortgages.server.login.LoginHelper;
import com.fertiletech.mortgages.server.messaging.EmailController;
import com.fertiletech.mortgages.server.messaging.MessagingDAO;
import com.fertiletech.mortgages.shared.DTOConstants;
import com.fertiletech.mortgages.shared.DuplicateEntitiesException;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class EntitySetup extends HttpServlet{
	private static final Logger log = Logger.getLogger(EntitySetup.class.getName());
	
	static{
		EntityDAO.registerClassesWithObjectify();
	}
	/**
	 * web.xml contains entries to ensure only registered developers for the app can execute
	 * this script. Does not go through login logic 
	 */
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		try
		{
			String type = req.getParameter("type");
			if(type == null)
			{
				out.println("<b><font color='red'>Please specify a type</font></b>");
				return;
			}
			out.println("<b>setup starting</b><br/>");
			if(type.equals("params"))
			{
				String result = createParameter();
				out.println(result);
				log.warning(result + " triggered by " + LoginHelper.getLoggedInUser(req));
			}
			else if(type.equals("email"))
			{
				String fromAddress = "customerservice@newprudential.com";
				String bccAddress = "map@newprudential.com";
		    	EmailController controller = MessagingDAO.createEmailController(MessagingDAO.PUBLIC_EMAILER, fromAddress, bccAddress, 5000, 95, 10000);
		    	log.warning("created: " + controller.getKey());
		    	controller = MessagingDAO.createEmailController(MessagingDAO.SYSTEM_EMAILER, fromAddress, null, 5000, 5, 1000);
		    	log.warning("created: " + controller.getKey());		    	
			}
			out.println("<b>setup done</b><br/>");
		}
		catch(DuplicateEntitiesException ex)
		{
			out.println("An error occured when creating objects: " + ex.getMessage());
		}
	}
		
	private static String createParameter() throws DuplicateEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		//params.put("sobulo@fertiletech.com", "added for support purposes during deployment of admin profiles");
		//String[] paramNames = {DTOConstants.APP_PARAM_ADMINS, DTOConstants.APP_PARAM_EDITOR, DTOConstants.APP_PARAM_REVIEWER};
		params.put("Current Residence", "");
		params.put("Property to be Purchased", "");
		String[] paramNames = {DTOConstants.APP_PARAM_MAP_TITLES};
		StringBuilder result = new StringBuilder();
		for(String name : paramNames)
		{
			ApplicationParameters paramObj = EntityDAO.createApplicationParameters(name, params);
			result.append("<p>Created: " + paramObj.getKey()+ "</p>");
		}
		return result.toString();
	}
}
