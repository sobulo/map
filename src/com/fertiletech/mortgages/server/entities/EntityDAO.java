package com.fertiletech.mortgages.server.entities;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.fertiletech.mortgages.server.ServiceImplUtilities;
import com.fertiletech.mortgages.server.login.LoginHelper;
import com.fertiletech.mortgages.server.messaging.EmailController;
import com.fertiletech.mortgages.server.messaging.MessagingDAO;
import com.fertiletech.mortgages.server.tasks.TaskQueueHelper;
import com.fertiletech.mortgages.shared.DTOConstants;
import com.fertiletech.mortgages.shared.DrivePickerUtils;
import com.fertiletech.mortgages.shared.DuplicateEntitiesException;
import com.fertiletech.mortgages.shared.LoginRoles;
import com.fertiletech.mortgages.shared.NPMBFormConstants;
import com.fertiletech.mortgages.shared.TableMessage;
import com.fertiletech.mortgages.shared.TableMessageHeader;
import com.fertiletech.mortgages.shared.TableMessageHeader.TableMessageContent;
import com.fertiletech.mortgages.shared.WorkflowStateInstance;
import com.google.api.server.spi.types.SimpleDate;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

public class EntityDAO {
	private static final Logger log =
	        Logger.getLogger(EntityDAO.class.getName());	
	private static boolean isOfyRegistered = false; 
	
	static
	{
		registerClassesWithObjectify();
	}
	
	//objectify requires we register all classes whose object instances will/can be 
	//persisted in appengine's native datastore
	
	public static void registerClassesWithObjectify()
	{
	    if(isOfyRegistered)
	        return;
	    
	    log.warning("Registering services with Objectify");
	    ObjectifyService.register(ApplicantUniqueIdentifier.class);
	    ObjectifyService.register(SalesLead.class);
	    ObjectifyService.register(MortgageComment.class);
	    ObjectifyService.register(MortgageApplicationFormData.class);
	    ObjectifyService.register(InetImageBlob.class);
	    ObjectifyService.register(ApplicationParameters.class);
	    ObjectifyService.register(SalesLeadIDHelper.class);
	    ObjectifyService.register(MortgageAttachment.class); 
	    ObjectifyService.register(EmailController.class);
	    isOfyRegistered = true;
	}
	
	public static Query<SalesLead> getSalesLeadsByDate(Date startDate, Date endDate)
	{
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(SalesLead.class).filter("dateCreated >=", startDate).
		filter("dateCreated <=", endDate);
	}	
	
	public static List<Key<SalesLead>> getConsumerLoanLeadKeysViaEmail(Objectify ofy, String email)
	{
		return ofy.query(SalesLead.class).
					ancestor(ApplicantUniqueIdentifier.getKey(email)).order("-dateCreated").listKeys();		
	}
	
	public static List<SalesLead> getConsumerLoanLeadViaEmail(Objectify ofy, String email)
	{
		return ofy.query(SalesLead.class).
					ancestor(ApplicantUniqueIdentifier.getKey(email)).list();		
	}
	
	private static TableMessageHeader getSalesLeadPublicHeader()
	{

			TableMessageHeader m = new TableMessageHeader(7);
			m.setText(0, "Amount", TableMessageContent.NUMBER);
			m.setText(1, "Equity", TableMessageContent.NUMBER);
			m.setText(2, "Tenor", TableMessageContent.NUMBER);
			m.setText(3, "Modified", TableMessageContent.DATE);
			m.setText(4, "Created", TableMessageContent.DATE);
			m.setText(5, "Status", TableMessageContent.TEXT);
			m.setText(6, "Message", TableMessageContent.TEXT);
			return m;		
	}
	
	private static TableMessageHeader getSalesLeadHeader()
	{
		TableMessageHeader h = new TableMessageHeader(10);
		h.setText(0, "M.A.P ID", TableMessageContent.NUMBER);
		h.setFormatOption(0, "###");
		h.setText(1, "Full Name", TableMessageContent.TEXT);
		h.setText(2, "Email", TableMessageContent.TEXT);
		h.setText(3, "Phone", TableMessageContent.TEXT);
		h.setText(4, "Company", TableMessageContent.TEXT);
		h.setText(5, "Annual Income", TableMessageContent.NUMBER);
		h.setText(6, "Loan Amount", TableMessageContent.NUMBER);
		h.setText(7, "Created", TableMessageContent.DATE);
		h.setText(8, "Modified", TableMessageContent.DATE);
		h.setText(9, "Status", TableMessageContent.TEXT);
		return h;
	}
	
	private static TableMessage getSalesLeadInfo(SalesLead lead)
	{
		TableMessage m = new TableMessage(5, 3, 2);
		m.setText(DTOConstants.FULL_NAME_IDX, lead.getFullName());
		m.setText(DTOConstants.EMAIL_IDX, lead.getEmail());
		m.setText(DTOConstants.PHONE_NUMBER_IDX, lead.getPhoneNumber());
		m.setText(DTOConstants.COMPANY_NAME_IDX, lead.getCompanyName());
		m.setText(DTOConstants.NOTE_IDX, lead.getCurrentState().getDisplayString());
		m.setNumber(DTOConstants.SALARY_IDX, lead.getAnnualSalary());
		m.setNumber(DTOConstants.LEAD_ID_IDX, lead.getLeadID());
		m.setNumber(DTOConstants.LOAN_AMT_IDX, lead.getLeadID());		
		m.setDate(DTOConstants.DATE_MODIFIED_IDX, lead.getDateUpdated());
		m.setDate(DTOConstants.DATE_CREATED_IDX, lead.getDateCreated());
		
		return m;
	}
	
	private static TableMessage getSalesLeadPublicInfo(SalesLead lead)
	{
		TableMessage m = new TableMessage(2, 3, 2);
		m.setText(DTOConstants.PUB_STATUS_IDX, lead.getCurrentState().getDisplayString());
		m.setText(DTOConstants.PUB_MSG_IDX, lead.getDisplayMessaage());
		m.setNumber(DTOConstants.PUB_LOAN_AMT_IDX, lead.getLoanAmount());
		m.setNumber(DTOConstants.PUB_EQUITY_IDX, lead.getEquity());
		m.setNumber(DTOConstants.PUB_TENOR_IDX, lead.getTenor());
		m.setDate(DTOConstants.PUB_MOD_IDX, lead.getDateUpdated());
		m.setDate(DTOConstants.PUB_MSG_IDX, lead.getDateCreated());		
		return m;
	}	
	
	public static List<TableMessage> getConsumerLeadsAsPublicTable(Iterable<SalesLead> leads)
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getSalesLeadPublicHeader());
		boolean okToCreateNew = true;
		for(SalesLead loanInfo : leads)
		{
			TableMessage loanRow = getSalesLeadPublicInfo(loanInfo);
			loanRow.setMessageId(loanInfo.getKey().getString());
			result.add(loanRow);
			okToCreateNew = okToCreateNew && loanInfo.allowNewSiblings();
		}
		result.get(0).setMessageId(String.valueOf(result.size() > 0 && okToCreateNew));
		return result;
	}
	
	public static List<TableMessage> getConsumerLeadsAsTable(Iterable<SalesLead> leads)
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getSalesLeadHeader());
		boolean okToCreateNew = true;
		for(SalesLead loanInfo : leads)
		{
			TableMessage loanRow = getSalesLeadInfo(loanInfo);
			loanRow.setMessageId(loanInfo.getKey().getString());
			result.add(loanRow);
			okToCreateNew = okToCreateNew && loanInfo.allowNewSiblings();
		}
		result.get(0).setMessageId(String.valueOf(okToCreateNew));
		return result;
	}	
	
	public static long getAllocatedSalesID()
	{
		return ObjectifyService.factory().allocateId(SalesLeadIDHelper.class);		
	}
	
	public static MortgageApplicationFormData getLoanFormData(Objectify ofy, Key<SalesLead> leadKey)
	{
		return ofy.get(MortgageApplicationFormData.getFormKey(leadKey));
	}
	
	public static MortgageApplicationFormData  getLoanFormData(String formKeyStr, Objectify ofy)
	{
		Key<? extends MortgageApplicationFormData> formKey = ofy.getFactory().stringToKey(formKeyStr);
		return getLoanFormData(formKey, ofy);
	}

	public static MortgageApplicationFormData  getLoanFormData(Key<? extends MortgageApplicationFormData> formKey, Objectify ofy)
	{
		MortgageApplicationFormData loanData = ofy.get(formKey);		
		return loanData;
	}
	
	public static MortgageComment createComment(Text comment, boolean showPublic, Key<SalesLead> leadKey, String user)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		MortgageComment loanComment = new MortgageComment(comment, leadKey, showPublic, user);
		try
		{
			ofy.put(loanComment);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanComment;
	}
	
	public static MortgageApplicationFormData createLoanLeadAndFormData(SalesLead salesLead, HashMap<String, String> formData, String updateUser) throws DuplicateEntitiesException
	{
		MortgageApplicationFormData loanApp = null;
		if(updateUser == null) updateUser = "anonymous user entry";
		Objectify ofy = ObjectifyService.beginTransaction();
		String email = formData.get(NPMBFormConstants.EMAIL);
		
		ApplicantUniqueIdentifier applicantIdentifier = new ApplicantUniqueIdentifier(email);
		List<Key<SalesLead>> leadKeys = getConsumerLoanLeadKeysViaEmail(ofy, email);
		try
		{
			setDecisionFields(salesLead, formData);
			if(leadKeys.size() == 0)
				ofy.put(applicantIdentifier, salesLead); //save lead first so we can get its auto-assigned key below
			else
			{
				Map<Key<SalesLead>, SalesLead> leads = ofy.get(leadKeys);
				boolean okToCreate = true;
				for(SalesLead ld : leads.values())
					okToCreate = okToCreate && ld.allowNewSiblings();
				if(okToCreate)
					ofy.put(salesLead);
				else
					throw new DuplicateEntitiesException("Looks like you've already started an application" +
						" with " + email + ". Please login first or click on the 'continue application'" +
								" link that was emailed to you at the time you applied");
			}
			loanApp = new MortgageApplicationFormData(salesLead.getKey(), formData);
			TaskQueueHelper.scheduleDelayedLoanAppStateChangeMessage(salesLead.getKey().getString());
			ofy.put(loanApp);
			ofy.getTxn().commit();			
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		//log differences
		WorkflowStateInstance state = WorkflowStateInstance.valueOf(formData.get(NPMBFormConstants.ID_STATE));
		String stateDescription  = state.toString() + " (" + state.getDisplayString() + ")";
		String[] comments = {"<b>Application " + salesLead.getKey().getParent().getName() + "/" + salesLead.getLeadID() +" updated. Current state is: " + stateDescription, ""};		
		StringBuilder diffs = new StringBuilder("<ul>");
		ServiceImplUtilities.addNewEntries(diffs, loanApp.getFormData(), loanApp.getSupplementaryData());
		comments[1] = diffs.toString();
		TaskQueueHelper.scheduleCreateComment(comments, salesLead.getKey().getString(), updateUser);		
		return loanApp;		
	}
	
	public static String changeState(Key<SalesLead> leadKey, WorkflowStateInstance newState, String message, String user)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		StringBuilder comments = new StringBuilder();
		try
		{
			SalesLead lead = ofy.get(leadKey);
			String oldMessage = lead.getDisplayMessaage()==null? "" : lead.getDisplayMessaage();
			boolean messageChanged = !message.equals(oldMessage);
			if(messageChanged)
			{
				comments.append("<p>Display message changed from <b>").append(oldMessage).append("</b> to <b>").append(message).append("</b></p>");
				lead.setDisplayMessage(message);
				
			}
			if(!newState.equals(lead.getCurrentState()))
			{
				MortgageApplicationFormData formData = ofy.get(MortgageApplicationFormData.getFormKey(leadKey));
				boolean submitted = true;
				comments.append("<p>State changed from <b>").append(lead.getCurrentState()).append("</b> to <b>").append(newState).append("</b></p>");
				if(newState.ordinal() <= WorkflowStateInstance.APPLICATION_CONFIRM.ordinal())
				{		
					submitted = false;
					if(!formData.isSubmmited())
						comments.append("<b>Application unlocked!</b>").append("User: ").append(lead.getEmail()).append(" can edit again");
				}
				lead.setCurrentState(newState);
				HashMap<String, String> formMap = formData.getFormData();
				formMap.put(NPMBFormConstants.ID_STATE, newState.toString());
				formData.submitted = submitted;
				ofy.put(lead, formData);
			}
			else if(messageChanged)
				ofy.put(lead);
			
			if(comments.length() > 0)
			{
				String[] commentString = {comments.toString()};
				TaskQueueHelper.scheduleCreateComment(commentString, leadKey.getString(), user);
			}
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return comments.toString();
	}
	
	public static MortgageApplicationFormData updateLoanLeadAndFormData(HashMap<String, String> formData, ArrayList<HashMap<String, String>>[] suppData, Key<SalesLead> leadKey, boolean submit, String updateUser)
	{
		SalesLead salesLead = null;		
		HashMap<String, String> oldForm = null;
		ArrayList<HashMap<String, String>>[] oldTables = null;
		Objectify ofy = ObjectifyService.beginTransaction();
		Key<MortgageApplicationFormData> loanKey = MortgageApplicationFormData.getFormKey(leadKey);
		MortgageApplicationFormData loanApp = null;
		try
		{ 
			loanApp = ofy.get(loanKey);
			oldForm = loanApp.getFormData();
			oldTables = loanApp.getSupplementaryData();
			salesLead = ofy.get(leadKey); //throws a runtime exception if not found
			setDecisionFields(salesLead, formData);
			WorkflowStateInstance newState = WorkflowStateInstance.valueOf(formData.get(NPMBFormConstants.ID_STATE));
			WorkflowStateInstance oldState = salesLead.getCurrentState();
			if(oldState.compareTo(newState) > 0)
				formData.put(NPMBFormConstants.ID_STATE, oldState.toString());			
			else
				salesLead.setCurrentState(newState);			
			loanApp.setFormData(formData, suppData, submit);
			
			ofy.put(loanApp, salesLead);			
			if(submit) 
			{
				if(salesLead.currentState.ordinal() >= WorkflowStateInstance.APPLICATION_CONFIRM.ordinal())
				{
					TaskQueueHelper.scheduleDelayedLoanAppStateChangeMessage(salesLead.getKey().getString());
				}		
			}
			ofy.getTxn().commit();
		}
		finally
		{			
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();		
		}
		//log differences
		String[] comments = {"<div style='border-bottom:1px dotted grey; margin: 2px; text-align:center'>Application " + salesLead.getKey().getParent().getName() + "/" + salesLead.getLeadID() +" updated. Current state is: " + formData.get(NPMBFormConstants.ID_STATE) + "</div>", ""};		
		StringBuilder diffs = new StringBuilder("<ul>");
		ServiceImplUtilities.addDifference(diffs, oldForm, formData, oldTables, suppData);
		comments[1] = diffs.toString();
		TaskQueueHelper.scheduleCreateComment(comments, salesLead.getKey().getString(), updateUser);

		return loanApp;
	}
	
	public static List<MortgageComment> getLoanComments(Objectify ofy, Key<SalesLead> leadKey)
	{
		if(ofy == null)
			ofy = ObjectifyService.begin();
		
		List<MortgageComment> list = ofy.query(MortgageComment.class).filter("loanKey", leadKey).list();
		log.warning("My list Size is: " + list.size());
		Collections.sort(list, Collections.reverseOrder()); //sort list in descending order
		log.warning("After sorting my list size is: " + list.size());
		return list;
	}		
	
	//comment array indicates auto-generation
	public static MortgageComment createComment(String[] commentList, boolean showPublic, Key<SalesLead> leadKey, String user)
	{
		StringBuilder joiner = new StringBuilder("<div>");
		for(String comment : commentList)
			joiner.append("<p>").append(comment).append("</p>");
		joiner.append("</div>");
		return createComment(new Text(joiner.toString()), showPublic, leadKey, user);
	}	
	
	private static void setDecisionFields(SalesLead salesLead, HashMap<String, String> formData)
	{
		Integer tenor = null;
		String tenorStr = formData.get(NPMBFormConstants.TENOR);
		
		if(tenorStr != null)
		{
			tenorStr = tenorStr.split(" ")[0];
			tenor = getNumberValue(tenorStr).intValue();
		}
		
		salesLead.setDecisionFields(formData.get(NPMBFormConstants.TEL_NO), formData.get(NPMBFormConstants.EMPLOYER),
											getFullName(formData), getNumberValue(formData.get(NPMBFormConstants.TOTAL_ANNUAL_PAY)),
											getNumberValue(formData.get(NPMBFormConstants.TOTAL_FINANCING)), 
											getNumberValue(formData.get(NPMBFormConstants.EQUITY_CONTRIBUTION)), getNumberValue(formData.get(NPMBFormConstants.LOAN_AMOUNT)), tenor, formData.get(NPMBFormConstants.FREQUENCY));
	}	
	
	private static String getFullName(HashMap<String, String> formData)
	{
		return (replaceNull(formData.get(NPMBFormConstants.SURNAME)) + " " + replaceNull(formData.get(NPMBFormConstants.FIRST_NAME))  
				+ " " + replaceNull(formData.get(NPMBFormConstants.MIDDLE_NAME))).trim();
	}
	
	private static String replaceNull(String val)
	{
		if(val == null)
			return "";
		else
			return val.trim();
	}
	
	private static Double getNumberValue(String numStr)
	{
		if(numStr == null) return null;
		Double num = null;
		try
		{
			num = Double.valueOf(numStr);
		}
		catch(NumberFormatException ex){}
		return num;
	}
	
	public static void sendLeadStateNotificationMessage(Key<SalesLead> leadKey, String updateUser)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
    	try
    	{
    		SalesLead salesLead = ofy.get(leadKey);
    		WorkflowStateInstance currentState = salesLead.getCurrentState();
    		String title = "NPMB Mortgage Application " + leadKey.getId() + " UPDATE " + currentState.getDisplayString();
    		String viewMailLink = "<p><a href='https://groups.google.com/a/newprudential.com/forum/#!searchin/map-emails/" + title + 
    				"'>View email sent to " + leadKey.getParent().getName() + "</a></p>";
    		String[] comments = {"<div style='background-color:grey;color-black;font-weight:bold;'>Sent email</div>", viewMailLink};
    		String[] emailMsg = getAppStateMessage(salesLead);
    		if(emailMsg != null)
    		{    			
    			MessagingDAO.scheduleSendEmail(leadKey.getParent().getName(), 
    					emailMsg, title, true);
    			TaskQueueHelper.scheduleCreateComment(comments, salesLead.getKey().getString(), updateUser);
    		}
    		ofy.getTxn().commit();
    	}
    	finally
    	{
    		if(ofy.getTxn().isActive())
    			ofy.getTxn().rollback();
    	}
	}
	
	public static ApplicationParameters createApplicationParameters(String id, HashMap<String, String> parameters) throws DuplicateEntitiesException
	{
		ApplicationParameters paramsObject= new ApplicationParameters(id, parameters);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<ApplicationParameters> existingKey = paramsObject.getKey();
			ApplicationParameters temp = ofy.find(existingKey);
			if(temp != null)
				ServiceImplUtilities.logAndThrowDuplicateException(log, "Application parameter already exists", existingKey);
			else
			{
				ofy.put(paramsObject);
				ofy.getTxn().commit();
			}
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return paramsObject;
	}	
	
	public static String[] getAppStateMessage(SalesLead lead)
	{
		String loanID = lead.getKey().getString();
		String name = (lead.getFullName() == null ? "" : lead.getFullName());
		String greet = "<p>Hello " + name + ",</p><br/>";
		String baseUrl = "http://apply.newprudential.com/";
		String signature = "<br/><p>Warmest regards,<br/>NPMB Customer Service</p>";
		
		String[] result = new String[2];
		switch(lead.getCurrentState())
		{
		case APPLICATION_STARTED:
			result[0] = "Hello " + name + "\nThank you for starting an online application on New Prudential Mortgage Bank's portal.\n\nWarmest Regards\nNPMB Customer Service";
			result[1] = greet + "Thank you for starting an online application on New Prudential Mortgage Bank's portal.<br/>" + 
			"<a href='" +  baseUrl + "#apply" + "/" + loanID + "'>" + "Click here to continue editing your " +
					"application</a>" + signature;
			return result;
		case APPLICATION_CONFIRM:
			String url = "<a href='" + baseUrl;
			String downloadUrl = url + "#print" + "/" + loanID + "'>here</a>";
			String uploadUrl = url + "#attach" + "/" + loanID + "'>here</a>";
			result[0] = "Hello " + name + "Thank you for submitting your application on New Prudential Mortgage Bank's portal." +
					" Visit apply.newprudential.com to download and ptint your application\n\nRegards\n,NPMB Customer Service";
			result[1] = greet +  "Thank you for submitting your application on New Prudential Mortgage Bank's portal. " + 
			"<p>Download and print a copy of your application " + downloadUrl + "<br/> " +
			"You can also upload supporting documentation " + uploadUrl + "</p>" + signature;
			return result;
		case APPLICATION_APPROVED:
			result[0] = "Hello " + name + "\nCongratulations, your mortgage application has been approved.\nWarmest Regards, \nNPMB Customer Service";
			result[1] = greet + "<p>Congratulations, your mortgage application has been approved. <a href='http://ww2.newprudential.com/contact-us'>Contact us</a> to find out what the" +
					" next steps are</p>" + signature;
			return result;
		case APPLICATION_DENIED:
			result[0] = "Hello " + name + "\nUnfortunately your application was denied.\n\nRegards, \nNPMB Customer Service";
			result[1] = greet + "<p>Unfortunately your application was denied. We recommend you visit " +
					"<a href='http://ww2.newprudential.com/our-products/deposit-products'>" +
					"our deposit products page</a> and sign up for any of the listed products. Doing so gets you started on " +
					"building a relationship with New Prudential Mortgage Bank.</p>Regards, <br/>NPMB Customer Service"; 
			return result;
		default:
			return null;
		}
	}
	
	public static List<MortgageAttachment> createAttachment(Key<SalesLead> leadKey, List<TableMessage> attachList, String user)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		List<MortgageAttachment> attachments = new ArrayList<MortgageAttachment>();
		try
		{
			boolean first = true;
			for(TableMessage m : attachList)
			{
				if(first) { first = false; continue; } //skip header
				MortgageAttachment attach = new MortgageAttachment(leadKey, m);
				attachments.add(attach);
			}
			ofy.put(attachments);
			String[] comment = {DrivePickerUtils.getAttachmentDescription(attachList)};
			TaskQueueHelper.scheduleCreateComment(comment, leadKey.getString(), user);
			ofy.getTxn().commit();			
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return attachments;
	}
	
	public static void removeAttachment(Key<MortgageAttachment> mtgAttachment, String user)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			MortgageAttachment attachment = ofy.get(mtgAttachment);
			String[] comments = {DrivePickerUtils.getAttachmentDescription(attachment.getDTO(), "REMOVED ATTACHMENT")};
			ofy.delete(mtgAttachment);
			TaskQueueHelper.scheduleCreateComment(comments, mtgAttachment.getParent().getString(), user);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
	private static TableMessageHeader getAttachmentHeader(Key<SalesLead> leadKey)
	{
		TableMessageHeader h = new TableMessageHeader(5);
		h.setText(0, "Title", TableMessageContent.TEXT);
		h.setText(1, "Type", TableMessageContent.TEXT);
		h.setText(2, "Drive Date", TableMessageContent.DATE);
		h.setText(3, "Attach Date", TableMessageContent.DATE);
		h.setText(4, "User", TableMessageContent.TEXT);
		h.setMessageId(leadKey.getString());
		h.setCaption("Attachments for M.A.P. ID: " + leadKey.getId());
		return h;
	}
	
	public static List<TableMessage> getAttachmentsDTO(Key<SalesLead> leadKey)
	{
		ArrayList<TableMessage> attachments = new ArrayList<TableMessage>();
		attachments.add(getAttachmentHeader(leadKey));
		for(MortgageAttachment a : getMortgageAttachments(leadKey))
			attachments.add(a.getDTO());
		return attachments;
	}
	
	private static List<MortgageAttachment> getMortgageAttachments(Key<SalesLead> leadKey)
	{
		return ObjectifyService.begin().query(MortgageAttachment.class).ancestor(leadKey).order("-attachDate").list();
	}
	
	public static List<TableMessage> getMapAttachments(Date startDate, Date endDate) {
		List<MortgageAttachment> mapList = ObjectifyService.begin().query(MortgageAttachment.class).
				filter("type", "MAPS").filter("attachDate >=", startDate).
				filter("attachDate <=", endDate).order("-attachDate").limit(400).list();
		ArrayList<TableMessage> attachments = new ArrayList<TableMessage>(mapList.size());
		HashSet<Key<SalesLead>> applicationKeys = new HashSet<Key<SalesLead>>();
		for(MortgageAttachment a : mapList)
			applicationKeys.add(a.getLeadKey());
		Map<Key<SalesLead>, SalesLead> applicaions = ObjectifyService.begin().get(applicationKeys);		
		for(MortgageAttachment a : mapList)
		{
			SalesLead lead = applicaions.get(a.getLeadKey());
			String attachDate = a.getAttachDate()==null? "" : String.format("%tc", a.getAttachDate());
			StringBuilder desc = new StringBuilder();
			String loanAmount = lead.getLoanAmount() == null? "" : EntityConstants.NUMBER_FORMAT.format(lead.getLoanAmount());
			String salary = lead.getAnnualSalary() == null? "" : EntityConstants.NUMBER_FORMAT.format(lead.getAnnualSalary());
			desc.append("<div style='width:180px;height:100px;color:#7b04a2;font-size:smaller'>").append("<a href='#doStatusCW/").
			append(a.getKey().getParent().getString()).append("'>View ID# ").
			append(String.valueOf(a.getKey().getParent().getId())).append("</a>").
			append("<br/>Annual Income: [").append(salary).append("]<br/>Loan Amount: [").append(loanAmount).
			append("]<br/>State: [").append(lead.getCurrentState().getDisplayString()).
			append("]<br/>Company: [").append(lead.getCompanyName()).
			append("]<br/>Date: [").append(attachDate).
			append("]</div>");
			
			TableMessage m = new TableMessage(2,2,0);
			m.setText(DTOConstants.CATEGORY_IDX, a.getTitle());
			m.setText(DTOConstants.DETAIL_IDX, desc.toString());
			m.setNumber(DTOConstants.LAT_IDX, a.getLatitude());
			m.setNumber(DTOConstants.LNG_IDX, a.getLongitude());
			m.setMessageId(a.getKey().getParent().getString());
			attachments.add(m);
		}
		return attachments;
	}	
	
}
