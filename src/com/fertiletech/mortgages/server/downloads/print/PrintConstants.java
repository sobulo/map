package com.fertiletech.mortgages.server.downloads.print;

import java.util.HashMap;

import com.fertiletech.mortgages.shared.NPMBFormConstants;

public class PrintConstants {
	public static HashMap<String, String> PDFMappings = new HashMap<String, String>();
	static
	{
		PDFMappings.put(NPMBFormConstants.EMPLOYER, "Name/Address of Employer (if less than three years, give name of previous employer)");
		PDFMappings.put(NPMBFormConstants.RESIDENTIAL_TYPE, "Specify");
	}
}
