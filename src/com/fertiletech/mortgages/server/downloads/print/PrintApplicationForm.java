/**
 * 
 */
package com.fertiletech.mortgages.server.downloads.print;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fertiletech.mortgages.server.downloads.PDFGenerationHelper;
import com.fertiletech.mortgages.server.entities.EntityConstants;
import com.fertiletech.mortgages.server.entities.EntityDAO;
import com.fertiletech.mortgages.server.entities.InetImageBlob;
import com.fertiletech.mortgages.server.entities.MortgageApplicationFormData;
import com.fertiletech.mortgages.server.entities.SalesLead;
import com.fertiletech.mortgages.shared.DTOConstants;
import com.fertiletech.mortgages.shared.NPMBFormConstants;
import com.fertiletech.mortgages.shared.TableMessage;
import com.fertiletech.mortgages.shared.TableMessageHeader;
import com.fertiletech.mortgages.shared.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.pdfjet.Box;
import com.pdfjet.Cell;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Image;
import com.pdfjet.ImageType;
import com.pdfjet.Letter;
import com.pdfjet.Line;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Point;
import com.pdfjet.RGB;
import com.pdfjet.Table;
import com.pdfjet.TextLine;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class PrintApplicationForm extends HttpServlet {

	static
	{
		EntityDAO.registerClassesWithObjectify();
	}
	private static final Logger log = Logger.getLogger(PrintApplicationForm.class
			.getName());
	private final static String disclaimerBegin = "Please note that fraudulent activity is punishable under Banks and Other Financial Institution Act (BOFIA 1991) " +
			"I hereby confirm applying for the above credit facility ("; 
	
	private final static String disclaimerEnds = ") and certify that all the information provided by me" +
			" above and attached thereto is true, correct and complete. I authorize you to make any enquiry you consider necessary and appropriate for the purpose of evaluating this application.";
	private final static String HEADER_MESSAGE = "<center><table border='0' align='left' cellspacing=20 width='80%'><tr><td><img src='http://www.addosser.com/addosser-doc-logo.jpg'/></td>" + 
											"<td valign='middle'><b>ADDOSSER MICROFINANCE LOAN APPLICATION FORM</b></td></tr></table><hr /></center>";
	
    private final static int RIGHT_MARGIN = 40;
    private final static int TOP_MARGIN = 40;
    private final static int ITEM_COL_WIDTH = 170;
    private final static int NOTES_COL_WIDTH = 225;
    private final static int AMOUNT_COL_WIDTH = 75;
    private final static int PADDING_SIZE = 5;
    private final static int UNDER_LINE_SIZE =  1;
    private final static int SPACE_BTW_BOXES = 12;
    private final static int HEADER_FONT_SIZE = 12;
    private final static int REGULAR_FONT_SIZE = 10;
    private final static int SMALL_FONT_SIZE = 8;
    private final static float REQ_REP_WIDTH = 550;
    public final static double LEFT_EDGE = REQ_REP_WIDTH + RIGHT_MARGIN;
 
    private final static int BOX_STUDENT_INFO_HEIGHT = 80;
    private final static int BOX_ADDRESS_INFO_HEIGHT = 50;
    private final static int BOX_COMMENT_INFO_HEIGHT = 80;
    private final static int BOX_OFFICIAL_INFO_HEIGHT = 45;
    private final static int BOX_DEFAULT_COMPANY_HEIGHT = 65;
     
    private final static String SESS_PREFIX = "fertiletech.";
	private final static String APP_FORM_OP = "appform";
	private final static String OP_TYPE = "type";
	private final static String LOAN_ID_PARAM ="form";
	private final static String COMPANY_INFO_WEB = "www.newprudential.com";
	private final static String COMPANY_INFO_ADDR = "21 Ahmed Onibudo Street. Victoria Island, Lagos.";
	private final static String COMPANY_INFO_EMAIL = "info@newprudential.com";
	private final static String COMPANY_INFO_NUM = "+234 1-454-1411, +234-1-454-1412";
	
	public final static double[] NEW_PRU_PURPLE = {114/255.0, 28/255.0, 103/255.0};

	/**
	 * web.xml contains entries to ensure only registered developers for the app
	 * can execute this script. Does not go through login logic
	 */

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		ServletOutputStream out = res.getOutputStream();
		
		String loanId = req.getParameter(LOAN_ID_PARAM);
		String type = req.getParameter(OP_TYPE);

		if (loanId == null || loanId.trim().length() == 0) {
			out.println("<b><font color='red'>No resource requested</font></b></body></html>");
			return;
		}
		Objectify ofy = ObjectifyService.begin();		
		HttpSession sess = req.getSession();
		if(APP_FORM_OP.equals(type))
		{
    		HashMap<String, String> data = (HashMap<String, String>) sess.getAttribute(getAppFormDataName(loanId));
    		ArrayList<HashMap<String, String>>[] suppData = (ArrayList<HashMap<String,String>>[]) sess.getAttribute(getAppSuppDataName(loanId));

	    	if(data == null || data.size() == 0)
	    	{
	    		res.setContentType("text/html");
	    		res.getOutputStream().println("<h1>Downloads</h1><br/><hr>");
	    		res.getOutputStream().println("<a href='" + getApplicationFormLink(loanId, req) + "'>Application Form</a>"); 
	    		return;    		
	    	}    	
			String fileName = "npmb-map-" + ObjectifyService.factory().stringToKey(loanId).getParent().getId() + ".pdf";
	        res.setContentType("application/octet-stream");        
	        res.setHeader("Content-disposition", "attachment; filename=" + fileName);	        
	        writeAppFormToStream(ofy, out, data, suppData);	    	
			
		}
	}

	private static void writeAppFormToStream(Objectify ofy, OutputStream out, HashMap<String, String> data,
			ArrayList<HashMap<String, String>>[] suppData)
    {    	    	
    	//init pdf file
    	try
    	{        	
	        PDF pdf = new PDF(out);
	        	        
	        //logo stream
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, DTOConstants.NPMB_FORM_HEADER);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	    	printAppForm(data, suppData, bis, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.fertile"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
        
    private static void printAppForm(HashMap<String, String> data, ArrayList<HashMap<String, String>>[] suppData,
    		BufferedInputStream imageStream, PDF pdf) throws Exception
    {
        Font regularFontBold = new Font(pdf, CoreFont.HELVETICA_BOLD);
        regularFontBold.setSize(SMALL_FONT_SIZE+2);
        Font smallFont = new Font(pdf, CoreFont.HELVETICA);
        smallFont.setSize(SMALL_FONT_SIZE-2);
    	
    	//setup additional fonts
        Font headerFont = new Font(pdf, CoreFont.HELVETICA_BOLD);
        headerFont.setSize(HEADER_FONT_SIZE);
        
        Font regularFont = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        regularFont.setSize(SMALL_FONT_SIZE + 2); 

	    //new page for student report card		    
	    Page page = new Page(pdf, Letter.PORTRAIT);
        
        //draw logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG); 
        //logo.scaleBy(0.75);        
 	    double maxWidth = 550;
	    logo.setPosition(RIGHT_MARGIN + (PADDING_SIZE * 2), TOP_MARGIN);
	    logo.drawOn(page);
	    double barHeight = 25;

	    //draw passport box
	    double boxHeight = logo.getHeight() + PADDING_SIZE + barHeight;
	    Box passportBox = new Box((RIGHT_MARGIN + maxWidth) - (boxHeight + PADDING_SIZE), TOP_MARGIN + PADDING_SIZE, boxHeight, boxHeight);
	    passportBox.drawOn(page);
	    String recent = "please affix recent";
	    TextLine passportText = new TextLine(smallFont, recent);
	    double baseY = TOP_MARGIN + (boxHeight/2 - PADDING_SIZE);
	    double baseX = (RIGHT_MARGIN + maxWidth) - boxHeight/2 - smallFont.stringWidth(recent)/2;
	    passportText.setPosition(baseX, baseY);
	    passportText.drawOn(page);
	    String clr = "color passport photograph";
	    passportText = new TextLine(smallFont, clr);
	    baseX = (RIGHT_MARGIN + maxWidth) - boxHeight/2 - smallFont.stringWidth(clr)/2;
	    passportText.setPosition(baseX, baseY + PADDING_SIZE + SMALL_FONT_SIZE);
	    passportText.drawOn(page);
	    
	    //draw mortgate text
	    clr = "NEW PRUDENTIAL MORTGAGE BANK";
	    passportText = new TextLine(headerFont, clr);
	    baseX = RIGHT_MARGIN + logo.getWidth() + (PADDING_SIZE * 4);
	    baseY = TOP_MARGIN + logo.getHeight() - barHeight;
	    passportText.setColor(RGB.WHITE);
	    passportText.setPosition(baseX + PADDING_SIZE, baseY + PADDING_SIZE + headerFont.getSize());
	    passportBox = new Box(baseX, baseY, headerFont.stringWidth(clr) + (PADDING_SIZE * 2), barHeight);
	    passportBox.setColor(NEW_PRU_PURPLE);
	    passportBox.setFillShape(true);
	    passportBox.drawOn(page);
	    passportText.drawOn(page);
	    clr = "HOME OWNERSHIP MORTGAGE LOAN APPLICATION FORM";
	    passportText = new TextLine(headerFont, clr);
	    baseX = RIGHT_MARGIN;
	    baseY = TOP_MARGIN + logo.getHeight() + PADDING_SIZE;
	    passportText.setColor(RGB.WHITE);
	    passportText.setPosition(baseX + PADDING_SIZE, baseY + PADDING_SIZE + headerFont.getSize());	    
	    passportBox = new Box(baseX, baseY, headerFont.stringWidth(clr) + (PADDING_SIZE * 2), barHeight);
	    passportBox.setColor(NEW_PRU_PURPLE);
	    passportBox.setFillShape(true);
	    passportBox.drawOn(page);
	    passportText.drawOn(page);
	    
	    baseY = baseY + barHeight + PADDING_SIZE + smallFont.getSize() + 2;
	    StringBuilder contactMsg = new StringBuilder();
	    String filler = "   ";
	    contactMsg.append("ADDRESS: ").append(COMPANY_INFO_ADDR).append(filler).append("TELEPHONE: ").append(COMPANY_INFO_NUM).
	    	append(filler).append("WEBSITE: ").append(COMPANY_INFO_WEB).append(filler).append(" EMAIL: ").append(COMPANY_INFO_EMAIL);
	    String contact = contactMsg.toString();
	    passportText = new TextLine(smallFont, contact);
	    passportText.setPosition(RIGHT_MARGIN + (maxWidth - smallFont.stringWidth(contact))/2, baseY);
	    passportText.setColor(NEW_PRU_PURPLE);
	    passportText.drawOn(page);
	    baseY += PADDING_SIZE;
	    Line headerLine = new Line(RIGHT_MARGIN, baseY, RIGHT_MARGIN + maxWidth, baseY);
	    headerLine.setWidth(2);
	    headerLine.setColor(NEW_PRU_PURPLE);
	    headerLine.drawOn(page);


	    final double yOffset = PADDING_SIZE + REGULAR_FONT_SIZE + UNDER_LINE_SIZE;
	    baseY = baseY + REGULAR_FONT_SIZE + yOffset + REGULAR_FONT_SIZE;
	    
	    
	    
	    double edgeX = RIGHT_MARGIN + maxWidth;
	    double edgeY = 730;
	    Point cursor = new Point();
	    cursor.setPosition(RIGHT_MARGIN, baseY);
	    printLine("SECTION A:     PARTICULARS OF BORROWER", false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset + PADDING_SIZE);
	    Point startPoint = new Point();
	    copyPoint(cursor, startPoint);
	    printFormKey(data, "Name", edgeX, edgeY, 1, false, cursor, regularFont, page, pdf);
	    String surname = data.get(NPMBFormConstants.SURNAME);
	    String firstname = data.get(NPMBFormConstants.FIRST_NAME);
	    String middlename = data.get(NPMBFormConstants.MIDDLE_NAME);
	    surname = surname==null?"":surname;
	    firstname = firstname==null?"":firstname;
	    middlename = middlename==null?"":middlename;
	    double base1 = startPoint.getX() + regularFont.stringWidth("Name: ");
	    double base2 =  base1 + maxWidth/3; 
	    double base3 = base2 + maxWidth/3.5; 

	    double top1 = base1 + maxWidth/6 - regularFont.stringWidth(surname)/2;
	    double top2 = base2 + maxWidth/6 - regularFont.stringWidth(middlename)/2;
	    double top3 = base3 + maxWidth/6 - regularFont.stringWidth(firstname)/2;
	    double under1 = base1 + maxWidth/6 - smallFont.stringWidth("Surname")/2;
	    double under2 = base2 + maxWidth/6 - smallFont.stringWidth("First Name")/2;
	    double under3 = base3 + maxWidth/6 - smallFont.stringWidth("Middle Name")/2;
	    final double underOffset = 4.0 + smallFont.getSize();
	    cursor.setPosition(top1, startPoint.getY());
	    printLine(surname, false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
	    cursor.setPosition(under1, startPoint.getY() + underOffset);
	    printLine("Surname", false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
	    cursor.setPosition(top2, startPoint.getY());
	    printLine(firstname, false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
	    cursor.setPosition(under2, startPoint.getY() + underOffset);	    
	    printLine("First Name", false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
	    cursor.setPosition(top3, startPoint.getY());
	    printLine(middlename, false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
	    cursor.setPosition(under3, startPoint.getY() + underOffset);	    
	    printLine("Middle Name", false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
	    printFormKey(data, NPMBFormConstants.ADDRESS, edgeX, edgeY, 1, true, cursor, regularFont, page, pdf);
	    underlineRegion(cursor.getX(), cursor.getY(), maxWidth/2, page);
	    cursor.setPosition(cursor.getX()+ maxWidth/2 + PADDING_SIZE, cursor.getY());
	    printFormKey(data, NPMBFormConstants.TEL_NO, edgeX, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.DATE_OF_BIRTH, RIGHT_MARGIN + maxWidth/3, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.PLACE_OF_BIRTH, RIGHT_MARGIN + maxWidth/1.6, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.GENDER, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.ORIGIN_STATE, RIGHT_MARGIN + maxWidth/1.6, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.NATIONALITY, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.EDUCATION, RIGHT_MARGIN + maxWidth/2, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.PROFESSION, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    StringBuilder employInfo = new StringBuilder(data.get(NPMBFormConstants.EMPLOYER));
	    employInfo.append(". ").append(data.get(NPMBFormConstants.EMPLOYER_ADDRESS));
	    String prior = data.get(NPMBFormConstants.EMPLOYER_PRIOR_NAME);
	    if(prior != null && prior.trim().length() > 0)
	    {
	    	employInfo.append(". ").append(NPMBFormConstants.EMPLOYER_PRIOR_NAME).append(": ").append(data.get(NPMBFormConstants.EMPLOYER_PRIOR_NAME)).
	    	append(". ").append(data.get(NPMBFormConstants.EMPLOYER_PRIOR_ADDERSS)).append("(").append(data.get(NPMBFormConstants.EMPLOYER_PRIOR_YEARS)).append(" yrs)");
	    }
	    
	    data.put(NPMBFormConstants.EMPLOYER, employInfo.toString());	    		
	    printFormKey(data, NPMBFormConstants.EMPLOYER, LEFT_EDGE, edgeY, 3, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.POSITION, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.OFFICE_NO, RIGHT_MARGIN + maxWidth/2, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.MOBILE_NO, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.FAX, RIGHT_MARGIN + maxWidth/2, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.OFFICE_EMAIL, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.EMPLOYER_YEARS, RIGHT_MARGIN + maxWidth/2, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.RETIREMENT, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.RESIDENTIAL_ADDRESS, edgeX, edgeY, 1, true, cursor, regularFont, page, pdf);
	    underlineRegion(cursor.getX(), cursor.getY(), maxWidth/2, page);
	    cursor.setPosition(cursor.getX()+ maxWidth/2 + PADDING_SIZE, cursor.getY());
	    printFormKey(data, NPMBFormConstants.RESIDENTIAL_TEL_NO, edgeX, edgeY, 1, true, cursor, regularFont, page, pdf);
	    
	    boolean isRented, isOwned, isOther;
	    isRented = isOwned = isOther = false;
	    String type = data.get(NPMBFormConstants.RESIDENTIAL_TYPE);
	    if(type.equals(NPMBFormConstants.RESIDENCE_TYPES_LIST[0]))
	    	isOwned = true;
	    else if(type.equals(NPMBFormConstants.RESIDENCE_TYPES_LIST[1]))
	    	isRented = true;
	    else
	    	isOther = true;
	    
	    printLine("Type: ", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFont, page);
	    double checkOffset = 15;
	    double checkGap = regularFont.getSize();
	    drawCheckBox(cursor, "Owned", isOwned, checkGap, regularFont, page);
	    cursor.setX(cursor.getX() + checkOffset);
	    drawCheckBox(cursor, "Rented", isRented, checkGap, regularFont, page);
	    cursor.setX(cursor.getX() + checkOffset);
	    drawCheckBox(cursor, "Other", isOther, checkGap, regularFont, page);
	    String key = "Specify";
	    if(isOther)
	    	key = NPMBFormConstants.RESIDENTIAL_TYPE;
	    printFormKey(data, key, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.ANNUAL_RENT, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    
	    type = data.get(NPMBFormConstants.MARITAL_STATUS);
	    boolean isMarried, isSingle, isDivorced;
	    isMarried = isSingle = isDivorced = false;
	    if(type != null)
	    {
		    if(type.equals(NPMBFormConstants.MARITAL_STATUS_LIST[0]))
		    	isMarried = true;
		    else if(type.equals(NPMBFormConstants.MARITAL_STATUS_LIST[1]))
		    	isSingle = true;
		    else if(type.equals(NPMBFormConstants.MARITAL_STATUS_LIST[2]))
		    	isDivorced = true;
	    }
	    printLine("Marital Status: ", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFont, page);
	    checkOffset = 50;
	    drawCheckBox(cursor, "Married", isMarried, checkGap, regularFont, page);
	    cursor.setX(cursor.getX() + checkOffset);
	    drawCheckBox(cursor, "Single", isSingle, checkGap, regularFont, page);
	    cursor.setX(cursor.getX() + checkOffset);
	    drawCheckBox(cursor, "Divorced/Separated", isDivorced, checkGap, regularFont, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
	    printFormKey(data, NPMBFormConstants.SPOUSE_NAME, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.SPOUSE_ADDRESS, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.SPOUSE_EMPLOYER, RIGHT_MARGIN + maxWidth/2, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.SPOUSE_YEARS, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.SPOUSE_PROFESSION, RIGHT_MARGIN + maxWidth/2, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.SPOUSE_INCOME, true, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printLine("Children/Other Dependents: ", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFont, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + PADDING_SIZE);
	    Page pageBookmark = page;
	    page = drawSupplementaryTable(suppData, NPMBFormConstants.DEPENDENT_IDX, cursor, maxWidth, 5, regularFont, regularFontBold, page, pdf);
	    printLine("Next-of-Kin:", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
	    page = printFormKey(data, NPMBFormConstants.NEXT_OF_KIN, RIGHT_MARGIN + maxWidth/3, edgeY, 1, false, cursor, regularFont, pageBookmark, pdf);
	    page = printFormKey(data, NPMBFormConstants.RELATIONSHIP, RIGHT_MARGIN + maxWidth/1.5, edgeY, 1, false, cursor, regularFont, pageBookmark, pdf);
	    page = printFormKey(data, NPMBFormConstants.KIN_AGE, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, pageBookmark, pdf);
	    page = printFormKey(data, NPMBFormConstants.KIN_ADDRESS, LEFT_EDGE, edgeY, 2, true, cursor, regularFont, pageBookmark, pdf);
	    if(pageBookmark == page)
	    {
	    	page = new Page(pdf, Letter.PORTRAIT);
	    	cursor.setPosition(RIGHT_MARGIN, TOP_MARGIN);
	    }
	    printLine("SECTION B:     PERSONAL FINANCIAL INFORMATION", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
	    printLine("Employment Income:", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFont, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
	    printFormKey(data, NPMBFormConstants.TOTAL_ANNUAL_PAY, RIGHT_MARGIN + maxWidth/2, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.MONTHLY_GROSS_PAY, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.MONTHLY_NET_PAY, RIGHT_MARGIN + maxWidth/2, edgeY, 1, true, cursor, regularFont, page, pdf);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
	    printLine("Other Periodic Pay from Employment:", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + PADDING_SIZE);
	    page = drawSupplementaryTable(suppData, NPMBFormConstants.OTHER_PERIODIC_IDX, cursor, maxWidth, 5, regularFont, regularFontBold, page, pdf);
	    printLine("Other Source(s) of Income:", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + PADDING_SIZE);	    
	    page = drawSupplementaryTable(suppData, NPMBFormConstants.OTHER_INCOME_IDX, cursor, maxWidth, 4, regularFont, regularFontBold, page, pdf);
	    printLine("Assets/Investments Owned (Real Estate, Equities, Bonds, Vehicles, etc):", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + PADDING_SIZE);	    	    
	    page = drawSupplementaryTable(suppData, NPMBFormConstants.ASSET_IDX, cursor, maxWidth, 5, regularFont, regularFontBold, page, pdf);
	    printLine("Existing Obligations/Liabilities (off payroll only):", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + PADDING_SIZE);	    	    
	    page = drawSupplementaryTable(suppData, NPMBFormConstants.OBLIGATION_IDX, cursor, maxWidth, 5, regularFont, regularFontBold, page, pdf);
	    printLine("Banking Details (Account Information):", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + PADDING_SIZE);	    	    
	    page = drawSupplementaryTable(suppData, NPMBFormConstants.BANKING_IDX, cursor, maxWidth, 5, regularFont, regularFontBold, page, pdf);
	    page = printFormKey(data, NPMBFormConstants.MONTHLY_EXPENSES, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset+ PADDING_SIZE);
	    printLine("SECTION C:     PARTICULARS OF THE LOAN", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + PADDING_SIZE + yOffset);
	    printFormKey(data, NPMBFormConstants.TOTAL_FINANCING, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.EQUITY_CONTRIBUTION, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.LOAN_AMOUNT, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.TENOR, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, NPMBFormConstants.FREQUENCY, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printLine("Proposed Security for the Loan", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFont, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + PADDING_SIZE);
	    page = drawSupplementaryTable(suppData, NPMBFormConstants.SECURITY_IDX, cursor, maxWidth, 5, regularFont, regularFontBold, page, pdf);
	    cursor.setX(cursor.getX() + regularFont.getSize());
	    
	    type = data.get(NPMBFormConstants.GUARANTEE_TYPE);
	    isOther = true;
	    if(type.equals(NPMBFormConstants.GUARANTEE_TYPE_LIST[0]))
	    {
	    	type = "";
	    	isOther = false;
	    }
	    String guaranty = "Guarantee of Employer";
	    drawBigCheckBox(cursor, guaranty, !isOther, regularFont, page);
	    checkGap = 50;
	    cursor.setX(cursor.getX() + checkGap + regularFont.stringWidth(guaranty));
	    guaranty = "Other Guarantee (Specify)";
	    drawBigCheckBox(cursor, guaranty, isOther, regularFont, page);
	    cursor.setX(cursor.getX() + PADDING_SIZE + regularFont.stringWidth(guaranty));
	    copyPoint(cursor, startPoint);
	    printLine(type, false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFont, page);
	    underlineRegion(startPoint.getX(), startPoint.getY(), LEFT_EDGE - startPoint.getX(), page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset + PADDING_SIZE*3);
	    printLine("SECTION D:     APPLICANT'S DECLARATION", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFontBold, page);
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
	    String[] nextPage = printLine(NPMBFormConstants.DECLARATION, false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor, regularFont, page);
	    if(nextPage[0] != null)
	    {
	    	page = new Page(pdf, Letter.PORTRAIT);
	    	cursor.setPosition(RIGHT_MARGIN, TOP_MARGIN);
	    	printLine(nextPage[0], false, RIGHT_MARGIN, LEFT_EDGE - regularFont.getSize() - PADDING_SIZE, edgeY, cursor, regularFont, page);
	    }
	    cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
	    key = "Full Name";
	    data.put(key, firstname + " " + middlename + " " + surname);
	    printFormKey(data, key, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	    printFormKey(data, "Signature", RIGHT_MARGIN + maxWidth/2, edgeY, 1, false, cursor, regularFont, page, pdf);
	    printFormKey(data, "Date", LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
    }
    
    
    private static ArrayList<TableMessage> getSupplementaryTable(ArrayList<HashMap<String, String>> supplementaryData, String[][] configTable, int defaultRows) throws NumberFormatException, Exception
    {   	
    	ArrayList<TableMessage> result = new ArrayList<TableMessage>(supplementaryData.size() + 1 + defaultRows);
    	//build header
    	TableMessageHeader header = new TableMessageHeader(configTable[0].length);
    	int textFields, doubleFields, dateFields;
    	textFields = doubleFields = dateFields = 0;
    	boolean addFooter = false;
    	
    	for(int h = 0; h < configTable[0].length; h++)
    	{
    		TableMessageContent type = TableMessageContent.valueOf(configTable[3][h]);
    		header.setText(h, configTable[0][h], type);
    		switch (type) {
			case DATE:
				dateFields++;
				break;
			case NUMBER:
				addFooter = true;
				doubleFields++;
				break;
			case TEXT:
				textFields++;
				break;
			default:
				break;
			}
    	}
    	result.add(header);

    	TableMessage footer = new TableMessage(textFields, doubleFields, dateFields);
    	footer.setText(0, "Total");
    	boolean invalidFooter = false;
    	for(int i = 0; i < supplementaryData.size(); i++)
    	{
    		TableMessage m = new TableMessage(textFields, doubleFields, dateFields);
    		HashMap<String, String> rowData = supplementaryData.get(i);
        	textFields = doubleFields = dateFields = 0;
    		for(int j = 0; j < header.getNumberOfHeaders(); j++)
    		{
    			TableMessageContent type = header.getHeaderType(j);
    			String val = rowData.get(header.getText(j));
    			switch(type)
    			{
				case DATE:
					dateFields++;
					throw new RuntimeException("Dates not currently supported");
				case NUMBER:
					Double dVal = null;
					if(val != null)
					{
						try
						{
							dVal = Double.valueOf(val);
							if(!invalidFooter)
							{
								if(footer.getNumber(doubleFields) == null)
									footer.setNumber(doubleFields, dVal);
								else
									footer.setNumber(doubleFields, dVal + footer.getNumber(doubleFields));
							}
						}
						catch(NumberFormatException ex)
						{invalidFooter = true;}
					}
					else
						invalidFooter = true;
					m.setNumber(doubleFields++, dVal);
					break;
				case TEXT:
					m.setText(textFields++, val);
					break;
				default:
					break;
    			
    			}
    		}
    		result.add(m);
    	}
    	
    	for(int i = supplementaryData.size(); i < defaultRows; i++)    	
    		result.add(new TableMessage(textFields, doubleFields, dateFields));
    	
    	if(addFooter)
    		result.add(footer);
    	
    	return result;
    }
    
    private static Page drawSupplementaryTable(ArrayList<HashMap<String, String>>[] supplementaryData, int idx,
    		Point cursor, double maxWidth, int defaultRows, Font f, Font h, Page page, PDF pdf) throws Exception
    {
	    Table table = new Table(h, f);;
		

	    //form table settings
	    List<TableMessage> tableData = getSupplementaryTable(supplementaryData[idx], NPMBFormConstants.ALL_CONFIG[idx], defaultRows);
    	List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(tableData, h, f, null);      	 		    
	    
	    table.setData(itemizedBillDesc, Table.DATA_HAS_0_HEADER_ROWS);
	    table.setLineWidth(0.2);
	    table.setPosition(cursor.getX(), cursor.getY());
	    table.setCellPadding(PADDING_SIZE/2.0);
	    table.rightAlignNumbers();
	    for(int i = 0; i < NPMBFormConstants.ALL_CONFIG[idx][1].length; i++)
	    {
	    	double width = maxWidth * Double.valueOf(NPMBFormConstants.ALL_CONFIG[idx][1][i])/100.0;
	    	table.setColumnWidth(i, width);
	    }
	    log.warning("WIDTH 2: " + table.getWidth());
	    
	    
	    //print payments table for this deposit
	    while(true)
	    {
		    Point tableEnd = table.drawOn(page);
		    if(!table.hasMoreData())
		    {
		    	cursor.setY(tableEnd.getY() + f.getSize() + h.getSize());
		    	break;
		    }
		    else
		    {
		    	page = new Page(pdf, Letter.PORTRAIT);
		    	cursor.setPosition(RIGHT_MARGIN, TOP_MARGIN);
		    	table.setPosition(RIGHT_MARGIN, TOP_MARGIN);
		    }
	    }
	    return page;
    }
    
    public static void drawCheckBox(Point cursor, String text, boolean checked, double gap, Font f, Page page) throws Exception
    {
    	Point startPoint = new Point();
	    printLine(text, cursor.getX(), cursor.getY(), f, page, null);
	    cursor.setX(cursor.getX() + gap + f.stringWidth(text));
	    copyPoint(cursor, startPoint);
	    startPoint.setRadius(f.getSize()/2);
	    startPoint.setY(startPoint.getY() - f.getSize()/2 + 2);
	    startPoint.setShape(Point.BOX);
	    startPoint.drawOn(page);
	    if(checked)
	    {
		    startPoint.setRadius(f.getSize()/2 - 2);
		    startPoint.setShape(Point.X_MARK);
		    startPoint.drawOn(page);
	    }
	    cursor.setX(cursor.getX() + f.getSize() + PADDING_SIZE);
    }
    
    public static void drawBigCheckBox(Point cursor, String text, boolean checked, Font f, Page page) throws Exception
    {
    	Point startPoint = new Point();
	    copyPoint(cursor, startPoint);
	    startPoint.setRadius(f.getSize()/3 * 2);
	    startPoint.setY(startPoint.getY() - f.getSize()/2 + 2);
	    startPoint.setShape(Point.BOX);
	    startPoint.drawOn(page);
	    if(checked)
	    {
		    startPoint.setRadius((f.getSize()/3 *2) - 2);
		    startPoint.setShape(Point.X_MARK);
		    startPoint.drawOn(page);
	    }
	    cursor.setX(cursor.getX() + f.getSize() + PADDING_SIZE);
	    printLine(text, cursor.getX(), cursor.getY(), f, page, null);
    }    

    public static Page printFormKey(HashMap<String, String> data, String key, double edgeX, double edgeY, double lineCount, boolean nextLine, Point cursor, Font regularFont, Page page, PDF pdf) throws Exception
    {
    	return printFormKey(data, key, false, edgeX, edgeY, lineCount, nextLine, cursor, regularFont, page, pdf);
    }
    
    public static Page printFormKey(HashMap<String, String> data, String key, boolean format, double edgeX, double edgeY, double lineCount, boolean nextLine, Point cursor, Font regularFont, Page page, PDF pdf) throws Exception
    {
	    String result = printLine(getLabel(key), false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page)[0];
	   
	    if(result != null)
	    {
	    	page = new Page(pdf, Letter.PORTRAIT);
	    	result = printLine(getLabel(key), false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page)[0];
	    }
	    
	    cursor.setPosition(cursor.getX() + PADDING_SIZE, cursor.getY());
	    String val = data.get(key);
	    if(format && val != null && val.trim().length() > 0)
	    	val = EntityConstants.NUMBER_FORMAT.format(Double.valueOf(val));
	    
	    String resultArr[] = printLine(val, true, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
	    int countIdx = Integer.valueOf(resultArr[1]);
	    if(resultArr[0] != null)
	    {
	    	page = new Page(pdf, Letter.PORTRAIT);
	    	resultArr = printLine(resultArr[0], true, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
	    	countIdx += Integer.valueOf(resultArr[1]);
	    }
	    
	    while(countIdx < lineCount)
	    {
	    	cursor.setPosition(RIGHT_MARGIN, cursor.getY() + regularFont.getSize() + PADDING_SIZE + UNDER_LINE_SIZE);
		    underlineRegion(RIGHT_MARGIN, cursor.getY(), LEFT_EDGE - RIGHT_MARGIN, page);
		    countIdx++;
	    }
	    if(nextLine)
	    	cursor.setPosition(RIGHT_MARGIN, cursor.getY() + regularFont.getSize() + PADDING_SIZE);
	    return page;
    }
    
	public static String getApplicationFormLink(String loanID, HttpServletRequest req)
	{
		Key<MortgageApplicationFormData> formKey = ObjectifyService.factory().stringToKey(loanID);
		Key<SalesLead> leadKey = formKey.getParent();
		Objectify ofy = ObjectifyService.begin();
		Map<Key<Object>, Object> objMap = ofy.get(formKey, leadKey);
		MortgageApplicationFormData loanObj = (MortgageApplicationFormData) objMap.get(formKey);
		HashMap<String, String> formData = loanObj.getFormData();
		ArrayList<HashMap<String, String>>[] suppData = loanObj.getSupplementaryData();
		SalesLead lead = (SalesLead) objMap.get(leadKey);
        HttpSession sess = req.getSession();
        sess.setAttribute(getAppFormDataName(loanID), formData);
        sess.setAttribute(getAppSuppDataName(loanID), suppData);
        return "http://" + req.getHeader("Host") + "/print/form?" + LOAN_ID_PARAM + "=" + loanID + "&" + OP_TYPE + "=" + APP_FORM_OP;
	}
	
	public static String getLabel(String key)
	{
		if(key == null || key.length() == 0)
			return "";
		
		if(PrintConstants.PDFMappings.containsKey(key))
			key = PrintConstants.PDFMappings.get(key);
		
		return key + ": ";
	}

	private static void copyPoint(Point source, Point target)
	{
		target.setPosition(source.getX(), source.getY());
	}
	
    private static void printLine(String line, int xPos, int yPos, Font font, Page page) throws Exception
    {
    	printLine(line, xPos, yPos, font, page, null);
    }
    
    private final static int NUM_OF_EXIS_LOAN_ROWS = 3;

     
    private static void printLine(String line, double xPos, double yPos, Font font, Page page, double[] color) throws Exception
    {
    	if(line == null)
    		line = "{BLANK}";
	    
    	TextLine text = new TextLine(font, line);
	    text.setPosition(xPos, yPos);
	    
	    if(color != null)
	    	text.setColor(color);
	    
	    text.drawOn(page);
    }
	
	public static String getAppFormDataName(String id)
	{
		return SESS_PREFIX + id + "-formdata";
	}
	
	public static String getAppSuppDataName(String id)
	{
		return SESS_PREFIX + id + "-suppdata";
	}
	
	public static String[] printLine(String text, boolean underline, double beginX, double edgeX, double edgeY, Point p, Font f, Page page) 
			throws Exception
	{	
		if(text == null) text = "";
		String[] tokens = text.split(" ");
		int returnIdx = -1;
		StringBuilder outputBuffer = new StringBuilder();
		Point s = new Point(p.getX(), p.getY());
		int lineCount = 0;
		for(int i = 0; i < tokens.length; i++)
		{
			double x = p.getX() + f.stringWidth(tokens[i]) + f.stringWidth(" ");
			double y = p.getY();
			if( x > (LEFT_EDGE - PADDING_SIZE - f.getSize()))
			{
				double edge = edgeX - s.getX();
				edgeX = LEFT_EDGE; //we've exceeded bounds user wanted, so stretch to end of page
				TextLine line = new TextLine(f, outputBuffer.toString());
				outputBuffer.setLength(0); //empty the buffer
				line.setPosition(s.getX(), s.getY());
				line.drawOn(page);					
				if(underline)
					underlineRegion(s.getX(), s.getY(), edgeX - s.getX(), page);
				x = beginX;
				y = y + PADDING_SIZE + f.getSize(); //go to next line
				s.setPosition(x, y);
				lineCount++;
			}
			
			if(y > edgeY)
			{
				y = TOP_MARGIN;
				returnIdx = i;
				p.setPosition(x, y);
				break;
			}
			
			p.setPosition(x, y);
			if(outputBuffer.length() > 0) 
				outputBuffer.append(" ");
			outputBuffer.append(tokens[i]);	
			
		}
		
		if(returnIdx < 0)
		{
			TextLine line = new TextLine(f, outputBuffer.toString());
			line.setPosition(s.getX(), s.getY());
			line.drawOn(page);
			if(underline)
			{
				underlineRegion(s.getX(), s.getY(), edgeX - s.getX(), page);
				p.setPosition(edgeX + PADDING_SIZE, p.getY());
			}
			lineCount++;
			String[] result = {null, String.valueOf(lineCount)};
			return result;
		}
		outputBuffer.setLength(0);
		outputBuffer.append(tokens[returnIdx]);
		for(int i = returnIdx + 1; i < tokens.length; i++)
			outputBuffer.append(" ").append(tokens[i]);
		String[] result = {outputBuffer.toString(), String.valueOf(lineCount)};
		return result;
	}
	
	public static void underlineRegion(double x, double y, double length, Page page) throws Exception
	{
		Line l = new Line(x, y + UNDER_LINE_SIZE, x + length, y + UNDER_LINE_SIZE);
		l.setPattern("[1 2] 4");
		
		l.drawOn(page);
	}
	

}