package com.fertiletech.mortgages.apply;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.gwtbootstrap3.client.ui.constants.IconType;

import com.google.gwt.user.datepicker.client.CalendarUtil;

/*
 * #%L
 * GwtBootstrap3
 * %%
 * Copyright (C) 2013 GwtBootstrap3
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * @author Joshua Godi
 */
public class NameTokens {
    // General Pages
    public static final String WELCOME = "welcome";
    public static final String APPLY = "apply";
    public static final String PRINT = "print";
    public static final String UPLOAD = "attach";
    public static final String STATUS = "status";
    public static final String CALCULATOR = "calculator";

    private final static String URL1 = "http://ww2.newprudential.com/properties/belmont-estate";
    private final static String URL2 = "http://ww2.newprudential.com/properties/eagles-garden-estate";
    private final static String URL3 = "http://ww2.newprudential.com/properties/evergood-court";
    private final static String URL4 = "http://ww2.newprudential.com/properties/renaissance-court";    
    private final static String URL5 = "http://ww2.newprudential.com/properties/golddust-apartments";
    private final static String URL6 = "http://ww2.newprudential.com/properties/goodluck-jonathan-estate";
    private final static String URL7 = "http://ww2.newprudential.com/properties/maple-court";
    private final static String URL8 = "http://ww2.newprudential.com/properties/maple-court-ii";
    private final static String URL9 = "http://ww2.newprudential.com/contact-us";
    private final static String URL10 = "http://ww2.newprudential.com/about-us/history";
    private final static String URL11 = "http://ww2.newprudential.com/about-us/compilance";
   
    // Getters for UiBinders
    public final static HashMap<String, String> getPropertyNameMap()
    {
    	HashMap<String, String> result = new LinkedHashMap<String, String>();
    	result.put(URL1, "Belmont Estate");
    	result.put(URL2, "Eagle Gardens Estate");
    	result.put(URL3, "Evergood Court");
    	result.put(URL5, "Golddust Apartments");
    	result.put(URL6, "Goodluck Jonathan Estate");
    	result.put(URL7, "Maple Court");
    	result.put(URL8, "Maple Court II");
    	result.put(URL4, "Renaissance  Court");
    	return result;
    }
    
    public final static HashMap<String, String> getAboutUsMap()
    {
    	LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
    	result.put(URL9, "Customer support lines");
    	@SuppressWarnings("deprecation")
		Date d = new Date(91, 8, 1);
    	int days = CalendarUtil.getDaysBetween(d, new Date());
    	int years = Math.round(days/366.0f);
    	result.put(URL10, "Over " + years + " years of excellence");
    	result.put(URL11, "Compliance with banking regulations");
    	return result;
    }    
    
    public final static HashMap<String, IconType> getAboutIcons()
    {
    	LinkedHashMap<String, IconType> result = new LinkedHashMap<String, IconType>();
    	result.put(URL9, IconType.PHONE_SQUARE);
    	result.put(URL10, IconType.TROPHY);
    	result.put(URL11, IconType.GAVEL);
    	return result;
    }

	public static String getUrl1() {
		return URL1;
	}

	public static String getUrl2() {
		return URL2;
	}

	public static String getUrl3() {
		return URL3;
	}

	public static String getUrl4() {
		return URL4;
	}

	public static String getUrl5() {
		return URL5;
	}

	public static String getUrl6() {
		return URL6;
	}

	public static String getUrl7() {
		return URL7;
	}

	public static String getUrl8() {
		return URL8;
	}
	
	public static String getWelcome() {
		return WELCOME;
	}
}
