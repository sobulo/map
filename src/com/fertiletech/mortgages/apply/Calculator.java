package com.fertiletech.mortgages.apply;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.extras.bootbox.client.Bootbox;

import com.fertiletech.mortgages.apply.forms.FormUtils;
import com.fertiletech.mortgages.apply.formwidgets.BSChainedValidator;
import com.fertiletech.mortgages.apply.formwidgets.BSControlValidator;
import com.fertiletech.mortgages.apply.formwidgets.BSListBox;
import com.fertiletech.mortgages.apply.formwidgets.BSTextWidget;
import com.fertiletech.mortgages.apply.formwidgets.CurrencyBox;
import com.fertiletech.mortgages.apply.utils.table.TablesView;
import com.fertiletech.mortgages.shared.TableMessage;
import com.fertiletech.mortgages.shared.TableMessageHeader;
import com.fertiletech.mortgages.shared.TableMessageHeader.TableMessageContent;
import com.fertiletech.mortgages.shared.oauth.ClientUtils;
import com.fertiletech.mortgages.shared.oauth.ClientUtils.NPMBUserCookie;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;

public class Calculator extends Composite {

	private static CalculatorUiBinder uiBinder = GWT
			.create(CalculatorUiBinder.class);

	interface CalculatorUiBinder extends UiBinder<Widget, Calculator> {
	}

	@UiField
	TablesView repaymentsTable;
	@UiField
	BSTextWidget<String> mortgageBox;
	@UiField
	BSTextWidget<String> rateBox;
	@UiField
	BSListBox yearsBox;
	@UiField
	BSTextWidget<String> equityBox;
	@UiField
	BSListBox isAmountBox; // equity as an amount or percentage
	@UiField
	BSListBox isAnnuityBox;
	@UiField
	Button calculate;
	@UiField
	BSTextWidget<String> totalInterest;
	@UiField
	BSTextWidget<String> totalPrincipal;
	@UiField
	BSTextWidget<String> totalMonthly;
	@UiField
	CurrencyBox equityDisplayBox;
	@UiField
	Button download;
	
	public final static int MATURITY_IDX = 0;
	public final static int PRINCIPAL_IDX = 0;
	public final static int INTEREST_IDX = 1;
	public final static int MONTHLY_IDX = 2;
	public final static int BALANCE_IDX = 3;
	public final static int BALANCE_END_IDX = 4;

	public final static String[] EQUITY_OPTIONS = { "Equity as Amount",
			"Equity As Percentage" };
	public final static String[] REPAYMENT_OPTIONS = { "Annuity",
			"Reducing Balance" };	
	
	public Calculator() {
		initWidget(uiBinder.createAndBindUi(this));
		FormUtils.setupListBox(isAmountBox, EQUITY_OPTIONS);
		FormUtils.setupListBox(isAnnuityBox, REPAYMENT_OPTIONS);
		FormUtils.setupListBox(yearsBox, 1, 40, "Year", "Years");
		calculate.addClickHandler(new ClickHandler() {
		
			@Override
			public void onClick(ClickEvent event) {
				repaymentsTable.clear();
				if(validateFields())
					repaymentsTable.showTable(getLoanPayments());
				else
				{
					CustomerAppHelper.showErrorMessage("<p>Aborting mortgage payments calculation.<br/> " +
							"Correct highlighted errors in the form, then click the calculate button again</p>");					
				}
			}
		});
		ValueChangeHandler<String> equityHandler = new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				updateEquityDisplayField();
			}
		};
		download.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				List<TableMessage> data = repaymentsTable.getTableData();
				if(data.size() == 0)
				{
					CustomerAppHelper.showErrorMessage("Nothing to download. Click calculate first");
					return;
				}
				CustomerAppHelper.LOAN_MKT_SERVICE.fetchGenericExcelLink(data, repaymentsTable.getTableHeader(), new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						StringBuilder link = new StringBuilder("<p><a href='").append(result).append("'>CLICK HERE TO EXPORT REPAYMENTS TABLE TO AN EXCEL FILE</a></p>");
						Bootbox.alert(link.toString());
					}
					
					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("Unable to complete download. Server message is: " + caught.getMessage());
					}
				});
			}
		});
		equityBox.addValueChangeHandler(equityHandler);
		isAmountBox.addValueChangeHandler(equityHandler);
		mortgageBox.addValueChangeHandler(equityHandler);
		
	}

	private boolean usePercentageForEquity()
	{
		return isAmountBox.getSelectedIndex() == 1;
	}
	
	private void update()
	{
		download.setVisible(false);
		if(ClientUtils.alreadyLoggedIn() && NPMBUserCookie.getCookie().isOps())
			download.setVisible(true);
	}
	
	private void updateEquityDisplayField()
	{
		if(usePercentageForEquity())
		{
			Double amount = getAmount(equityBox);
			Double mtg = getAmount(mortgageBox);
			if(amount == null || mtg == null) return;
			equityDisplayBox.setAmount(amount/100 * mtg);
		}
		else
			equityDisplayBox.setValue(equityBox.getValue());
	}

	public boolean isSet(BSListBox lb) {
		return lb.getSelectedIndex() == 0;
	}

	public Double getAmount(BSTextWidget<String> currencyBox) {
		return FormUtils.getAmount(currencyBox);
	}
	
	public void setAmount(BSTextWidget<String> currencyBox, Double value)
	{
		CurrencyBox cb = (CurrencyBox) currencyBox.getFieldWidget();
		cb.setAmount(value);
	}

	private TableMessage getPaymentRow(Date maturity, Double principal,
			Double interest, Double startBalance, Double endBalance) {
		TableMessage result = new TableMessage(0, 5, 1);
		result.setDate(MATURITY_IDX, maturity);
		result.setNumber(PRINCIPAL_IDX, principal);
		result.setNumber(INTEREST_IDX, interest);
		result.setNumber(MONTHLY_IDX, principal + interest);
		result.setNumber(BALANCE_IDX, startBalance);
		result.setNumber(BALANCE_END_IDX, endBalance);
		return result;
	}

	private TableMessageHeader getPaymentHeader() {
		TableMessageHeader result = new TableMessageHeader(6);
		result.setText(0, "Date", TableMessageContent.DATE);
		result.setText(1, "Principal Portion", TableMessageContent.NUMBER);
		result.setText(2, "Interest Portion", TableMessageContent.NUMBER);
		result.setText(3, "Monthly Repayment", TableMessageContent.NUMBER);
		result.setText(4, "Starting Balance", TableMessageContent.NUMBER);
		result.setText(5, "Ending Balance", TableMessageContent.NUMBER);
		return result;
	}

	private ArrayList<TableMessage> getLoanPayments() {

		double loanPrincipal = getAmount(mortgageBox) - equityDisplayBox.getAmount();
		double interest = getAmount(rateBox) / (12*100);
		double period = Integer.valueOf(yearsBox.getValue()) * 12;

		double monthlyPayment = loanPrincipal
				* (interest / (1 - (Math.pow(1 + interest, -period))));
		double currentPrincipal = loanPrincipal;
		double principalPaid, interestPaid, beginningBalance;
		final double THRESH_HOLD = 0.01;

		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getPaymentHeader());
		Date dateCounter = new Date();
		final double periodPrincipal = loanPrincipal / period;
		double totalPrincipal, totalInterest;
		totalPrincipal = totalInterest = 0;
		while (currentPrincipal > THRESH_HOLD) {
			dateCounter = CalendarUtil.copyDate(dateCounter);
			CalendarUtil.addMonthsToDate(dateCounter, 1);
			beginningBalance = currentPrincipal;
			interestPaid = interest * currentPrincipal;
			principalPaid = isSet(isAnnuityBox) ? (monthlyPayment - interestPaid)
					: periodPrincipal;
			currentPrincipal = beginningBalance - principalPaid;
			result.add(getPaymentRow(dateCounter, principalPaid, interestPaid,
					beginningBalance, currentPrincipal));
			totalInterest += interestPaid;
			totalPrincipal += principalPaid;
		}
		setAmount(this.totalPrincipal, totalPrincipal);
		setAmount(this.totalInterest, totalInterest);
		setAmount(this.totalMonthly, totalPrincipal + totalInterest);
		
		return result;
	}
	
	public boolean validateFields()
	{
	
		BSControlValidator<String>[] validators = new BSControlValidator[2];
		validators[0] = new ValidateNumber(mortgageBox);
		boolean result = validators[0].markErrors();
		validators[0] = new ValidateNumber(rateBox);
		validators[1] = new ValidateRate();
		BSChainedValidator<String> validator = new BSChainedValidator<String>(validators);
		result = result && validator.markErrors();
		
		validators[0] = new ValidateNumber(equityBox);
		validators[1] = new ValidateEquity();
		validator = new BSChainedValidator<String>(validators);
		
		return result && validator.markErrors();
		
	}
	
	private class ValidateNumber extends BSControlValidator<String>
	{

		public ValidateNumber(BSTextWidget<String> widget) {
			super(widget, true, widget.getLabel() + " must have a value specified greater than 0");
		}

		@Override
		protected boolean validate() {
			Double amount = getAmount(controlWidget);
			if(amount == null || amount < 0)
				return false;
			return true;
		}		
	}
	
	private class ValidateEquity extends BSControlValidator<String>
	{

		public ValidateEquity() {
			super(equityBox, true, "Equity contribution value displayed below, must be less than value of Mortgage. Edit the amount or percentage specified to fix");
		}

		@Override
		protected boolean validate() {
			Double equity = equityDisplayBox.getAmount();
			Double mortgage = getAmount(mortgageBox);
			if(equity == null || mortgage == null)
				return false;
			else return equity < mortgage;
		}
	}
	
	private class ValidateRate extends BSControlValidator<String>
	{

		public ValidateRate() {
			super(rateBox, true, "Rate must fall between 0% and 100%");
		}

		@Override
		protected boolean validate() {
			Double rate = getAmount(rateBox);
			if(rate == null)
				return false;
			return rate > 0 && rate < 100;
		}
		
	}
}
