/**
 * 
 */
package com.fertiletech.mortgages.apply.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface GeneralImageBundle extends ClientBundle{
	
	@Source("logo.png")
	ImageResource logoSmall();

	@Source("cirbelmont.png")
	ImageResource thumb1();
	@Source("circeagle.png")
	ImageResource thumb2();	
	@Source("circevergood.png")
	ImageResource thumb3();
	@Source("circrennais.png")
	ImageResource thumb4();
	@Source("circsimplicio.png")
	ImageResource thumb5();
	@Source("circgoodluck.png")
	ImageResource thumb6();
	@Source("maplecir.png")
	ImageResource thumb7();
	@Source("maplecourtcir.png")
	ImageResource thumb8();

	@Source("belmontroll.png")
	ImageResource thumb1b();
	@Source("eaglesroll.png")
	ImageResource thumb2b();	
	@Source("evergoodroll.png")
	ImageResource thumb3b();
	@Source("renaissanceroll.png")
	ImageResource thumb4b();
	@Source("simplicioroll.png")
	ImageResource thumb5b();
	@Source("gjonathanroll.png")
	ImageResource thumb6b();
	@Source("maple2roll.png")
	ImageResource thumb7b();
	@Source("maple1roll.png")
	ImageResource thumb8b();

	@Source("logob.jpg")
	ImageResource home();
	@Source("joysmaller.jpg")
	ImageResource homeb();
	@Source("calc.jpg")
	ImageResource homec();
	
}
