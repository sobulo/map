package com.fertiletech.mortgages.apply.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.TabContent;
import org.gwtbootstrap3.client.ui.TabListItem;
import org.gwtbootstrap3.client.ui.TabPane;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;
import org.gwtbootstrap3.extras.bootbox.client.Bootbox;
import org.gwtbootstrap3.extras.bootbox.client.callback.ConfirmCallback;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerView;

import com.fertiletech.mortgages.apply.CustomerAppHelper;
import com.fertiletech.mortgages.apply.NameTokens;
import com.fertiletech.mortgages.apply.formwidgets.BSControlValidator;
import com.fertiletech.mortgages.apply.formwidgets.BSDatePicker;
import com.fertiletech.mortgages.apply.formwidgets.BSLoanFieldValidator;
import com.fertiletech.mortgages.apply.formwidgets.BSTextWidget;
import com.fertiletech.mortgages.apply.formwidgets.CurrencyBox;
import com.fertiletech.mortgages.apply.formwidgets.FlexPanel;
import com.fertiletech.mortgages.shared.DTOConstants;
import com.fertiletech.mortgages.shared.FormValidator;
import com.fertiletech.mortgages.shared.NPMBFormConstants;
import com.fertiletech.mortgages.shared.WorkflowStateInstance;
import com.fertiletech.mortgages.shared.oauth.ClientUtils;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Geolocation.PositionOptions;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;

public class MortgageApplicationFormPanel extends Composite{

	@UiField
	TabListItem personalHeader;
	@UiField
	TabListItem employmentHeader;
	@UiField
	TabListItem familyHeader;
	@UiField
	TabListItem loanHeader;
	@UiField
	TabListItem financialHeader;
	@UiField
	TabListItem signatureHeader;
	@UiField
	TabPane personalBody;
	@UiField
	TabPane employmentBody;
	@UiField
	TabPane familyBody;
	@UiField
	TabPane financialBody;
	@UiField
	TabPane loanBody;
	@UiField
	TabPane signatureBody;
	
	@UiField
	FlexPanel dependentSlot;
	@UiField
	FlexPanel otherPeriodic;
	@UiField
	FlexPanel otherIncome;
	@UiField
	FlexPanel assets;
	@UiField
	FlexPanel obligations;
	@UiField
	FlexPanel banking;
	@UiField
	FlexPanel security;
	
	@UiField
	BSTextWidget<String> surname;
	@UiField
	BSTextWidget<String> firstName;
	@UiField
	BSTextWidget<String> middleName;
	@UiField
	BSTextWidget<String> employerAddress;
	@UiField
	BSTextWidget<String> residentialAddress;
	@UiField
	BSTextWidget<String> spouseAddress;
	@UiField
	BSTextWidget<String> mailingAddress;
	@UiField
	BSTextWidget<String> phone;
	@UiField
	BSTextWidget<String> alternateNo;	
	@UiField
	BSTextWidget<String> email;
	@UiField
	BSTextWidget<Date> dob;
	@UiField
	BSTextWidget<String> pob;
	@UiField
	BSTextWidget<String> gender;
	@UiField
	BSTextWidget<String> nationality;
	@UiField
	BSTextWidget<String> origin;
	@UiField
	BSTextWidget<String> profession;
	@UiField
	BSTextWidget<String> employer;
	@UiField
	BSTextWidget<String> position;
	@UiField
	BSTextWidget<String> employmentYears;
	@UiField
	BSTextWidget<String> employerNo;
	@UiField
	BSTextWidget<String> employerEmail;
	@UiField
	BSTextWidget<String> retirementYears;
	@UiField
	BSTextWidget<String> employerFax;
	@UiField
	BSTextWidget<String> residenceType;
	@UiField
	BSTextWidget<String> maritalStatus;
	@UiField
	BSTextWidget<String> residencePhone;
	@UiField
	BSTextWidget<String> residenceOther;
	@UiField
	BSTextWidget<String> rent;
	@UiField
	BSTextWidget<String> spouseName;
	@UiField
	BSTextWidget<String> spousePhone;
	@UiField
	BSTextWidget<String> spouseEmail;
	@UiField
	BSTextWidget<String> spouseIncome;
	@UiField
	BSTextWidget<String> education;
	@UiField
	BSTextWidget<String> kinAddress;
	@UiField
	BSTextWidget<String> kinName;
	@UiField
	BSTextWidget<String> kinRelationship;
	@UiField
	BSTextWidget<String> kinAge;
	@UiField
	BSTextWidget<String> totalAnnualPay;
	@UiField
	BSTextWidget<String> monthlyGrossPay;
	@UiField
	BSTextWidget<String> monthlyNetPay;
	@UiField
	BSTextWidget<String> monthlyExpenses;
	@UiField
	BSTextWidget<String> totalFinancing;
	@UiField
	BSTextWidget<String> equityContribution;
	@UiField
	BSTextWidget<String> loanAmount;
	@UiField
	BSTextWidget<String> tenor;
	@UiField
	BSTextWidget<String> frequency;
	@UiField
	BSTextWidget<String> guaranteeType;
	@UiField
	Paragraph declarationContent;
	@UiField
	Small declarationAuthor;
	@UiField
	BSTextWidget<String> otherGuarantee;
	@UiField
	BSTextWidget<String> priorYears;
	@UiField
	BSTextWidget<String> priorEmployer;
	@UiField
	BSTextWidget<String> priorAddress;
	@UiField
	BSTextWidget<String> spouseYears;
	@UiField
	BSTextWidget<String> spouseProfession;
	@UiField
	BSTextWidget<String> spouseEmployer;

	@UiField
	Button personalSave;
	@UiField
	Button familySave;	
	@UiField
	Button employmentSave;
	@UiField
	Button financialSave;	
	@UiField
	Button loanSave;
	@UiField
	Button submit;
		
	String loanId = null;
	
	HashMap<Button, WorkflowStateInstance> buttonToStateMap = new HashMap<Button, WorkflowStateInstance>();
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> fetchFormDataCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(final HashMap<String, String> result) {
        	final String setEmail = email.getValue();
        	try
        	{
        		setFormData(result); 
        	}
        	catch(Exception e)
        	{
        		StringBuilder errorMessage = new StringBuilder("<b>Warning, a few fields might not have " +
        				"been loaded: " + e.getMessage() + "<b><hr><div style='font-size:smaller'><ul>");
        		for(String key :result.keySet())
        			errorMessage.append("<li>").append(result.get(key)).append("</li>");
        		errorMessage.append("</ul></div>");
				CustomerAppHelper.showErrorMessage(errorMessage.toString());
				
        	}
        	CustomerAppHelper.LOAN_MKT_SERVICE.getSupplementaryData(result.get(NPMBFormConstants.ID), new AsyncCallback<ArrayList<HashMap<String,String>>[]>() {

				@Override
				public void onFailure(Throwable caught) {
					fetchFormDataCallback.onFailure(caught);
				}

				@Override
				public void onSuccess(ArrayList<HashMap<String, String>>[] tables) {
					try
					{
					loanId = result.get(NPMBFormConstants.ID);
					boolean editable = Boolean.valueOf(result.get(NPMBFormConstants.ID_EDIT));
					setFormTables(tables);
					WorkflowStateInstance state = WorkflowStateInstance.valueOf(result.get(NPMBFormConstants.ID_STATE));
					enableTabs(state);
					activatePanel(state);
					//override editable, if an email set (assumption is this email set only if NOT ops/staff)
					if(editable)
						if(setEmail.trim().length() > 0 && !setEmail.equalsIgnoreCase(result.get(NPMBFormConstants.EMAIL)))
						{
							CustomerAppHelper.showInfoMessage("This app was created from a different email address, setting to read only mode. Login with " + setEmail +
									" and/or logout to edit the application form");
							editable = false;
						}
					enableEditing(editable);
					}
					catch(Exception e)
					{
						Window.alert("Warning, some table rows might not have been loaded: " + e.getMessage());
					}
				}
			});
        }

        @Override
        public void onFailure(Throwable caught) {
        	CustomerAppHelper.showErrorMessage("Unable to retrieve application form. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+ caught.getMessage() + "</b></p>");
        	enableButtons(false);
        }
    };  

	private AsyncCallback<String[]> submitCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
        	Bootbox.alert("Unable to submit application, please try again. Error was: " + caught.getMessage());
        	enableButtons(true);
		}

		@Override
		public void onSuccess(String[] result) {
        	Bootbox.alert("You application form has been submitted. If you need to make edits to the submitted application, please contact support");
        	boolean editable = Boolean.valueOf(result[DTOConstants.LOAN_EDIT_IDX]);
        	enableEditing(editable);
        	nextPanel(WorkflowStateInstance.APPLICATION_CONFIRM);
        	CustomerAppHelper.updateLatLngVals(loanId);
		}
	};

	private AsyncCallback<String[]> saveCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
        	Bootbox.alert("Unable to save values. Error was: " + caught.getMessage());
        	enableButtons(true);			
		}

		@Override
		public void onSuccess(String[] result) {
        	Bootbox.alert("Saved successfully. You may continue editing your application");
        	enableButtons(true);
        	WorkflowStateInstance state = WorkflowStateInstance.valueOf(result[DTOConstants.LOAN_STATE_IDX]);
        	nextPanel(state);
        	CustomerAppHelper.updateLatLngVals(loanId);
		}
	};
	
	private static MortgageApplicationFormPanelUiBinder uiBinder = GWT
			.create(MortgageApplicationFormPanelUiBinder.class);

	interface MortgageApplicationFormPanelUiBinder extends
			UiBinder<Widget, MortgageApplicationFormPanel> {
	}

	public MortgageApplicationFormPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		buttonToStateMap.put(personalSave, WorkflowStateInstance.APPLICATION_PERSONAL);
		buttonToStateMap.put(familySave, WorkflowStateInstance.APPLICATION_FAMILY);
		buttonToStateMap.put(financialSave, WorkflowStateInstance.APPLICATION_FINANCIAL);
		buttonToStateMap.put(loanSave, WorkflowStateInstance.APPLICATION_LOAN);
		buttonToStateMap.put(submit, WorkflowStateInstance.APPLICATION_CONFIRM);
		buttonToStateMap.put(employmentSave, WorkflowStateInstance.APPLICATION_EMPLOYMENT);
		FormUtils.setupFlexPanel(dependentSlot, NPMBFormConstants.DEPENDENT_TABLE_CFG);
		FormUtils.setupFlexPanel(otherPeriodic, NPMBFormConstants.PERIODIC_TABLE_CFG);
		FormUtils.setupFlexPanel(otherIncome, NPMBFormConstants.INCOME_SRC_TABLE_CFG);
		FormUtils.setupFlexPanel(banking, NPMBFormConstants.BANKING_TABLE_CFG);
		FormUtils.setupFlexPanel(assets, NPMBFormConstants.ASSET_TABLE_CFG);
		FormUtils.setupFlexPanel(security, NPMBFormConstants.SECURITY_TABLE_CFG);
		FormUtils.setupFlexPanel(obligations, NPMBFormConstants.OBLIGATION_TABLE_CFG);
		FormUtils.setupListBox(education, NPMBFormConstants.EDUCATION_LIST);
		FormUtils.setupListBox(residenceType, NPMBFormConstants.RESIDENCE_TYPES_LIST);
		FormUtils.setupListBox(priorYears, 1, 40, "Year", "Years");
		FormUtils.setupListBox(employmentYears, 1, 40, "Year", "Years");
		FormUtils.setupListBox(retirementYears, 1, 40, "Year", "Years");
		FormUtils.setupListBox(spouseYears, 1, 40, "Year", "Years");
		FormUtils.setupListBox(maritalStatus, NPMBFormConstants.MARITAL_STATUS_LIST);
		FormUtils.setupListBox(frequency, NPMBFormConstants.FREQUENCY_LIST);
		FormUtils.setupListBox(gender, NPMBFormConstants.GENDER_LIST);
		FormUtils.setupListBox(tenor, 1, 30, "Year", "Years");
		FormUtils.setupListBox(guaranteeType, NPMBFormConstants.GUARANTEE_TYPE_LIST);
		FormUtils.initOtherListCombo(guaranteeType, otherGuarantee);
		FormUtils.initOtherListCombo(residenceType, residenceOther);
		ValueChangeHandler<String> declarationHandler = new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				setDeclarationAuthor(getFormData());
				
			}
		};
		surname.addValueChangeHandler(declarationHandler);
		firstName.addValueChangeHandler(declarationHandler);
		middleName.addValueChangeHandler(declarationHandler);
		declarationContent.setText(NPMBFormConstants.DECLARATION);
		BSDatePicker dobBox = (BSDatePicker) dob.getFieldWidget();
		dobBox.setStartView(DateTimePickerView.DECADE);
		dobBox.setMinView(DateTimePickerView.MONTH);
		dobBox.setFormat("yyyy M d");
		dobBox.setValue(null);
		Date startDate = new Date();
		CalendarUtil.addDaysToDate(startDate, -(365 * 40));
		CalendarUtil.setToFirstDayOfMonth(startDate);
		CalendarUtil.resetTime(startDate);
		dobBox.setStartDate(startDate);
		dobBox.setAutoClose(true);
		dobBox.reload();

		//setup click handlers
		ClickHandler saveHandler = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				submitHandler((Button) event.getSource());
			}
		};
		
		ClickHandler submitHandler = new ClickHandler() {
			
			@Override
			public void onClick(final ClickEvent event) {
				final Button clickSource = (Button) event.getSource();
				String msg = "Click ok to confirm you have read and agree with the terms stated on this page";
				Bootbox.confirm(msg, new ConfirmCallback() {
					
					@Override
					public void callback(boolean result) {
						if(result)
							submitHandler(clickSource);
					}
				});
				
			}
		};
		
		setupValidators();
		setupLoanFieldHandlers();
		personalSave.addClickHandler(saveHandler);
		familySave.addClickHandler(saveHandler);
		employmentSave.addClickHandler(saveHandler);
		financialSave.addClickHandler(saveHandler);
		loanSave.addClickHandler(saveHandler);
		submit.addClickHandler(submitHandler);
	}
	
	private void setupLoanFieldHandlers()
	{
		ValueChangeHandler<String> loanAmountHandler = new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				Double enteredTotalFinance = FormUtils.getAmount(totalFinancing);
				Double enteredEquityContrib = FormUtils.getAmount(equityContribution);
				if(enteredTotalFinance == null || enteredEquityContrib == null)return;
				((CurrencyBox)loanAmount.getFieldWidget()).setAmount(enteredTotalFinance - enteredEquityContrib);
			}
		};
		totalFinancing.addValueChangeHandler(loanAmountHandler);
		equityContribution.addValueChangeHandler(loanAmountHandler);
	}
	
	public void enableTabs(WorkflowStateInstance state)
	{
		switch (state) {
		case APPLICATION_APPROVED:
		case APPLICATION_MEET_REQUIRED:
		case APPLICATION_REVIEW:
		case APPLICATION_PAUSED:
		case APPLICATION_PENDING:
		case APPLICATION_CALL:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		case APPLICATION_CONFIRM:
			signatureHeader.setEnabled(true);
		case APPLICATION_LOAN:
			loanHeader.setEnabled(true);
		case APPLICATION_FINANCIAL:
			financialHeader.setEnabled(true);
		case APPLICATION_FAMILY:
			familyHeader.setEnabled(true);
		case APPLICATION_EMPLOYMENT:
			employmentHeader.setEnabled(true);
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			personalHeader.setEnabled(true);
		}
	}
	
	public void activatePanel(WorkflowStateInstance state)
	{
		deactivatePanels();
		switch (state) {
		case APPLICATION_APPROVED:
		case APPLICATION_MEET_REQUIRED:
		case APPLICATION_REVIEW:
		case APPLICATION_PAUSED:
		case APPLICATION_PENDING:
		case APPLICATION_CALL:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		case APPLICATION_CONFIRM:
			signatureHeader.setActive(true);
			signatureBody.setActive(true);
			break;
		case APPLICATION_LOAN:
			loanHeader.setActive(true);
			loanBody.setActive(true);
			break;
		case APPLICATION_FINANCIAL:
			financialHeader.setActive(true);
			financialBody.setActive(true);
			break;
		case APPLICATION_FAMILY:
			familyHeader.setActive(true);
			familyBody.setActive(true);
			break;
		case APPLICATION_EMPLOYMENT:
			employmentHeader.setActive(true);
			employmentBody.setActive(true);
			break;
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			personalHeader.setActive(true);
			personalBody.setActive(true);
		}		
	}
	
	public void deactivatePanels()
	{
		signatureHeader.setActive(false);
		signatureBody.setActive(false);
		loanHeader.setActive(false);
		loanBody.setActive(false);
		financialHeader.setActive(false);
		financialBody.setActive(false);
		familyHeader.setActive(false);
		familyBody.setActive(false);
		employmentHeader.setActive(false);
		employmentBody.setActive(false);
		personalHeader.setActive(false);
		personalBody.setActive(false);
	}

	public void priorPanel(WorkflowStateInstance state)
	{
		switch (state) {
		case APPLICATION_CONFIRM:
			switchPanel(signatureHeader, loanHeader, signatureBody, loanBody);
			break;
		case APPLICATION_LOAN:
			switchPanel(loanHeader, financialHeader, loanBody, financialBody);
			break;
		case APPLICATION_FINANCIAL:
			switchPanel(financialHeader, familyHeader, financialBody, familyBody);
			break;
		case APPLICATION_FAMILY:
			switchPanel(familyHeader, employmentHeader, familyBody, employmentBody);
			break;
		case APPLICATION_EMPLOYMENT:
			switchPanel(employmentHeader, personalHeader, employmentBody, personalBody);
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
		case APPLICATION_APPROVED:
		case APPLICATION_MEET_REQUIRED:
		case APPLICATION_REVIEW:
		case APPLICATION_PAUSED:
		case APPLICATION_PENDING:
		case APPLICATION_CALL:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:			
		}		
	}
	
	public void nextPanel(WorkflowStateInstance state)
	{
		boolean allowJump = !ClientUtils.NPMBUserCookie.getCookie().isOps();
		switch (state) {
		case APPLICATION_CONFIRM:
			if(allowJump) History.newItem(NameTokens.PRINT + "/" + loanId);
			break;
		case APPLICATION_LOAN:
			switchPanelToNext(loanHeader, signatureHeader, loanBody, signatureBody);
			break;
		case APPLICATION_FINANCIAL:
			switchPanelToNext(financialHeader, loanHeader, financialBody, loanBody);
			break;
		case APPLICATION_FAMILY:
			switchPanelToNext(familyHeader, financialHeader, familyBody, financialBody);
			break;
		case APPLICATION_EMPLOYMENT:
			switchPanelToNext(employmentHeader, familyHeader, employmentBody, familyBody);
			break;
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			switchPanelToNext(personalHeader, employmentHeader, personalBody, employmentBody);
			break;
		case APPLICATION_APPROVED:
		case APPLICATION_MEET_REQUIRED:
		case APPLICATION_REVIEW:
		case APPLICATION_PAUSED:
		case APPLICATION_PENDING:			
		case APPLICATION_CALL:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:			
		}		
	}

	private void switchPanel(TabListItem currentHeader, TabListItem switchedHeader, TabPane currentBody, TabPane switchedBody)
	{
		currentHeader.setActive(false);
		currentBody.setActive(false);
		switchedHeader.setActive(true);
		switchedBody.setActive(true);
	}
	
	
	private void switchPanelToNext(TabListItem currentHeader, TabListItem nextHeader, TabPane currentBody, TabPane nextBody)
	{
		if(!currentHeader.isActive()) //something else is active, and yes this is a hack
			deactivatePanels();
		switchPanel(currentHeader, nextHeader, currentBody, nextBody);
		nextHeader.setEnabled(true);
	}
	
	public void submitHandler(Button b) 
	{
		HashMap<String, String> customerInfo = getFormData();
		customerInfo.put(NPMBFormConstants.ID_STATE, buttonToStateMap.get(b).toString());
		if(validateFields(b))
			submitFormData(customerInfo, b);
	}
	
	public void enableButtons(boolean enabled)
	{
		personalSave.setEnabled(enabled);
		employmentSave.setEnabled(enabled);
		familySave.setEnabled(enabled);
		financialSave.setEnabled(enabled);
		loanSave.setEnabled(enabled);
		submit.setEnabled(enabled);
	}
	
	private void submitFormData(HashMap<String, String> data, Button b)
	{
		GWT.log("Loan ID: " + loanId);
		ArrayList<HashMap<String, String>>[] suppData = getFormTables();
		enableButtons(false); //prevent user from hitting save multiple times while waiting on server
		if(b == submit)
			CustomerAppHelper.LOAN_MKT_SERVICE.saveLoanApplicationData(data, suppData, loanId, true, submitCallback);
		else
			CustomerAppHelper.LOAN_MKT_SERVICE.saveLoanApplicationData(data, suppData, loanId, false, saveCallback);
	}	
	
	private boolean validateFields(Button button)
	{
		errorMap.clear();
		TabContent x;
		
		ArrayList<BSControlValidator> validators;
		if(button == personalSave)
			validators = personalValidators;
		else if(button == employmentSave)
			validators = employmentValidators;
		else if(button == familySave)
			validators = familyValidators;
		else if(button == financialSave)
			validators = financialValidators;
		else if(button == loanSave)
			validators = loanValidators;
		else
		{
			validators = new ArrayList<BSControlValidator>();
			validators.addAll(personalValidators);
			validators.addAll(employmentValidators);
			validators.addAll(familyValidators);
			validators.addAll(financialValidators);
			validators.addAll(loanValidators);
		}
		
		for(BSControlValidator v : validators)
			v.markErrors();
		
		if(errorMap.size() > 0)
		{
			StringBuilder b = new StringBuilder(errorMap.size() + (errorMap.size()==1? " error" : " errors") + " found.<ul>");
			for(String val : errorMap.values())
				b.append("<li>").append(val).append("</li>");
			b.append("</ul>");
			CustomerAppHelper.showErrorMessage(b.toString());
		}
		return errorMap.size() == 0;
	}	
	
	ArrayList<BSControlValidator> personalValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> employmentValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> financialValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> familyValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> loanValidators = new ArrayList<BSControlValidator>();
	HashMap<String, String> errorMap = new HashMap<String, String>();

	private void setupValidators()
	{
		HashMap<String, BSTextWidget> controlWidgetsMap = new HashMap<String, BSTextWidget>();
    	controlWidgetsMap.put(NPMBFormConstants.EMAIL, email);
    	controlWidgetsMap.put(NPMBFormConstants.TEL_NO, phone);
    	controlWidgetsMap.put(NPMBFormConstants.SURNAME, surname);
    	controlWidgetsMap.put(NPMBFormConstants.FIRST_NAME, firstName);
    	controlWidgetsMap.put(NPMBFormConstants.RESIDENTIAL_ADDRESS, residentialAddress);
    	controlWidgetsMap.put(NPMBFormConstants.DATE_OF_BIRTH, dob);
    	controlWidgetsMap.put(NPMBFormConstants.MOBILE_NO, alternateNo);
    	
    	controlWidgetsMap.put(NPMBFormConstants.OFFICE_EMAIL, employerEmail);
    	controlWidgetsMap.put(NPMBFormConstants.OFFICE_NO, employerNo);
    	controlWidgetsMap.put(NPMBFormConstants.FAX, employerFax);    	
    	controlWidgetsMap.put(NPMBFormConstants.EMPLOYER, employer);
    	controlWidgetsMap.put(NPMBFormConstants.EMPLOYER_ADDRESS, employerAddress);
    	
    	controlWidgetsMap.put(NPMBFormConstants.KIN_ADDRESS, kinAddress);
    	controlWidgetsMap.put(NPMBFormConstants.NEXT_OF_KIN, kinName);
    	controlWidgetsMap.put(NPMBFormConstants.SPOUSE_EMAIL, spouseEmail);
    	controlWidgetsMap.put(NPMBFormConstants.SPOUSE_PHONE, spousePhone);
    	
    	controlWidgetsMap.put(NPMBFormConstants.TOTAL_ANNUAL_PAY, totalAnnualPay);
    	controlWidgetsMap.put(NPMBFormConstants.MONTHLY_NET_PAY, monthlyNetPay);
    	controlWidgetsMap.put(NPMBFormConstants.MONTHLY_GROSS_PAY, monthlyGrossPay);
    	controlWidgetsMap.put(NPMBFormConstants.MONTHLY_EXPENSES, monthlyExpenses);
    	
    	controlWidgetsMap.put(NPMBFormConstants.TOTAL_FINANCING, totalFinancing);
    	controlWidgetsMap.put(NPMBFormConstants.EQUITY_CONTRIBUTION, equityContribution);
    	controlWidgetsMap.put(NPMBFormConstants.LOAN_AMOUNT, loanAmount);
    	
    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_PERSONAL_VALIDATORS, personalValidators);
    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_EMPLOYMENT_VALIDATORS, employmentValidators);
    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_FAMILY_VALIDATORS, familyValidators);
    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_FINANCIAL_VALIDATORS, financialValidators);
    	setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_LOAN_VALIDATORS, loanValidators);
	}
	
	private void setupValidator(HashMap<String, BSTextWidget> controlWidgetsMap, HashMap<String, FormValidator[]> validationSet, ArrayList<BSControlValidator> targetValidatorList)
	{
		for(String formKey : validationSet.keySet())
		{
			FormValidator[] fieldValidator = validationSet.get(formKey);
			BSTextWidget inputField = controlWidgetsMap.get(formKey);
			for(FormValidator v : fieldValidator)
				if(v.equals(FormValidator.MANDATORY))
					inputField.markRequired();
			BSLoanFieldValidator submitValidator = new BSLoanFieldValidator(fieldValidator,inputField, formKey, errorMap, true);
			targetValidatorList.add(submitValidator);
		}
	}
	
	public ArrayList<HashMap<String, String>>[] getFormTables()
	{
		ArrayList<HashMap<String, String>>[] result = new ArrayList[7];
		result[NPMBFormConstants.DEPENDENT_IDX] = dependentSlot.getAllRows();
		result[NPMBFormConstants.OTHER_INCOME_IDX] = otherIncome.getAllRows();
		result[NPMBFormConstants.OTHER_PERIODIC_IDX] = otherPeriodic.getAllRows();
		result[NPMBFormConstants.ASSET_IDX] = assets.getAllRows();
		result[NPMBFormConstants.OBLIGATION_IDX] = obligations.getAllRows();
		result[NPMBFormConstants.SECURITY_IDX] = security.getAllRows();
		result[NPMBFormConstants.BANKING_IDX] = banking.getAllRows();
		return result;
	}
	
	public void setFormTables(ArrayList<HashMap<String, String>>[] tables)
	{
		
		if(tables == null)
		{
			GWT.log("Null Table");
			return;
		}
		GWT.log("Not Null Table");
		dependentSlot.initRows(tables[NPMBFormConstants.DEPENDENT_IDX]);
		otherIncome.initRows(tables[NPMBFormConstants.OTHER_INCOME_IDX]);
		otherPeriodic.initRows(tables[NPMBFormConstants.OTHER_PERIODIC_IDX]);
		assets.initRows(tables[NPMBFormConstants.ASSET_IDX]);
		obligations.initRows(tables[NPMBFormConstants.OBLIGATION_IDX]);
		security.initRows(tables[NPMBFormConstants.SECURITY_IDX]);
		banking.initRows(tables[NPMBFormConstants.BANKING_IDX]);
	}
	
	public HashMap<String, String> getFormData()
	{
		HashMap<String, String> result = new HashMap<String, String>();
		result.put(NPMBFormConstants.SURNAME, surname.getValue());
		result.put(NPMBFormConstants.FIRST_NAME, firstName.getValue());
		result.put(NPMBFormConstants.MIDDLE_NAME, middleName.getValue());
		result.put(NPMBFormConstants.EMPLOYER_ADDRESS, employerAddress.getValue());
		result.put(NPMBFormConstants.RESIDENTIAL_ADDRESS, residentialAddress.getValue());
		result.put(NPMBFormConstants.SPOUSE_ADDRESS, spouseAddress.getValue());
		result.put(NPMBFormConstants.ADDRESS, mailingAddress.getValue());
		result.put(NPMBFormConstants.TEL_NO, phone.getValue());		
		result.put(NPMBFormConstants.MOBILE_NO, alternateNo.getValue());
		result.put(NPMBFormConstants.EMAIL, email.getValue());
		result.put(NPMBFormConstants.DATE_OF_BIRTH, dob.getDisplayText());
		result.put(NPMBFormConstants.PLACE_OF_BIRTH, pob.getValue());
		result.put(NPMBFormConstants.GENDER, gender.getValue());		
		result.put(NPMBFormConstants.NATIONALITY, nationality.getValue());
		result.put(NPMBFormConstants.ORIGIN_STATE, origin.getValue());
		result.put(NPMBFormConstants.PROFESSION, profession.getValue());
		result.put(NPMBFormConstants.EMPLOYER, employer.getValue());		
		result.put(NPMBFormConstants.POSITION, position.getValue());
		result.put(NPMBFormConstants.EMPLOYER_YEARS, employmentYears.getValue());
		result.put(NPMBFormConstants.OFFICE_EMAIL, employerEmail.getValue());
		result.put(NPMBFormConstants.RETIREMENT, retirementYears.getValue());				
		result.put(NPMBFormConstants.FAX, employerFax.getValue());
		result.put(NPMBFormConstants.RESIDENTIAL_TYPE, residenceType.getValue());
		result.put(NPMBFormConstants.MARITAL_STATUS, maritalStatus.getValue());
		result.put(NPMBFormConstants.SPOUSE_PROFESSION, spousePhone.getValue());
		result.put(NPMBFormConstants.SPOUSE_EMPLOYER, spouseEmployer.getValue());
		result.put(NPMBFormConstants.SPOUSE_YEARS, spouseYears.getValue());
		result.put(NPMBFormConstants.EMPLOYER_TEL, employerNo.getValue());
		result.put(NPMBFormConstants.EMPLOYER_PRIOR_NAME, priorEmployer.getValue());
		result.put(NPMBFormConstants.EMPLOYER_PRIOR_YEARS, priorYears.getValue());
		result.put(NPMBFormConstants.EMPLOYER_PRIOR_ADDERSS, priorAddress.getValue());
		if(residenceOther.getValue() != null && residenceOther.getValue().trim().length() > 0)
			result.put(NPMBFormConstants.RESIDENTIAL_TYPE, residenceOther.getValue());
		result.put(NPMBFormConstants.ANNUAL_RENT, rent.getValue());
		result.put(NPMBFormConstants.SPOUSE_NAME, spouseName.getValue());				
		result.put(NPMBFormConstants.SPOUSE_INCOME, spouseIncome.getValue());
		result.put(NPMBFormConstants.SPOUSE_EMAIL, spouseEmail.getValue());
		result.put(NPMBFormConstants.SPOUSE_PHONE, spousePhone.getValue());
		result.put(NPMBFormConstants.EDUCATION, education.getValue());		
		result.put(NPMBFormConstants.KIN_ADDRESS, kinAddress.getValue());
		result.put(NPMBFormConstants.NEXT_OF_KIN, kinName.getValue());
		result.put(NPMBFormConstants.RELATIONSHIP, kinRelationship.getValue());
		result.put(NPMBFormConstants.KIN_AGE, kinAge.getValue());		
		result.put(NPMBFormConstants.TOTAL_ANNUAL_PAY, totalAnnualPay.getValue());
		result.put(NPMBFormConstants.MONTHLY_GROSS_PAY, monthlyGrossPay.getValue());
		result.put(NPMBFormConstants.MONTHLY_NET_PAY, monthlyNetPay.getValue());
		result.put(NPMBFormConstants.TOTAL_FINANCING, totalFinancing.getValue());		
		result.put(NPMBFormConstants.TOTAL_ANNUAL_PAY, totalAnnualPay.getValue());
		result.put(NPMBFormConstants.MONTHLY_GROSS_PAY, monthlyGrossPay.getValue());
		result.put(NPMBFormConstants.MONTHLY_NET_PAY, monthlyNetPay.getValue());
		result.put(NPMBFormConstants.TOTAL_FINANCING, totalFinancing.getValue());		
		result.put(NPMBFormConstants.MONTHLY_EXPENSES, monthlyExpenses.getValue());
		result.put(NPMBFormConstants.EQUITY_CONTRIBUTION, equityContribution.getValue());
		result.put(NPMBFormConstants.TENOR, tenor.getValue());
		result.put(NPMBFormConstants.FREQUENCY, frequency.getValue());				
		result.put(NPMBFormConstants.LOAN_AMOUNT, loanAmount.getValue());
		result.put(NPMBFormConstants.GUARANTEE_TYPE, guaranteeType.getValue());
		if(otherGuarantee.getValue() != null && otherGuarantee.getValue().trim().length() != 0)
			result.put(NPMBFormConstants.GUARANTEE_TYPE, otherGuarantee.getValue());
		
		return result;
	}
	
	public void setFormData(HashMap<String, String> result)
	{
		surname.setValue(result.get(NPMBFormConstants.SURNAME));
		firstName.setValue(result.get(NPMBFormConstants.FIRST_NAME));
		middleName.setValue(result.get(NPMBFormConstants.MIDDLE_NAME));
		employerAddress.setValue(result.get(NPMBFormConstants.EMPLOYER_ADDRESS));
		residentialAddress.setValue(result.get(NPMBFormConstants.RESIDENTIAL_ADDRESS));
		spouseAddress.setValue(result.get(NPMBFormConstants.SPOUSE_ADDRESS));
		mailingAddress.setValue(result.get(NPMBFormConstants.ADDRESS));
		phone.setValue(result.get(NPMBFormConstants.TEL_NO));
		alternateNo.setValue(result.get(NPMBFormConstants.MOBILE_NO));
		email.setValue(result.get(NPMBFormConstants.EMAIL));
		dob.setDisplayText(result.get(NPMBFormConstants.DATE_OF_BIRTH));
		pob.setValue(result.get(NPMBFormConstants.PLACE_OF_BIRTH));
		gender.setValue(result.get(NPMBFormConstants.GENDER));		
		email.setValue(result.get(NPMBFormConstants.EMAIL));
		nationality.setValue(result.get(NPMBFormConstants.NATIONALITY));
		origin.setValue(result.get(NPMBFormConstants.ORIGIN_STATE));
		profession.setValue(result.get(NPMBFormConstants.PROFESSION));
		employer.setValue(result.get(NPMBFormConstants.EMPLOYER));		
		position.setValue(result.get(NPMBFormConstants.POSITION));
		employmentYears.setValue(result.get(NPMBFormConstants.EMPLOYER_YEARS));
		employerEmail.setValue(result.get(NPMBFormConstants.OFFICE_EMAIL));
		retirementYears.setValue(result.get(NPMBFormConstants.RETIREMENT));				
		employerFax.setValue(result.get(NPMBFormConstants.FAX));
		FormUtils.setOtherListCombo(residenceType, residenceOther, result.get(NPMBFormConstants.RESIDENTIAL_TYPE));
		rent.setValue(result.get(NPMBFormConstants.ANNUAL_RENT));
		spouseName.setValue(result.get(NPMBFormConstants.SPOUSE_NAME));
		spouseIncome.setValue(result.get(NPMBFormConstants.SPOUSE_INCOME));
		spouseEmail.setValue(result.get(NPMBFormConstants.SPOUSE_EMAIL));
		spousePhone.setValue(result.get(NPMBFormConstants.SPOUSE_PHONE));
		education.setValue(result.get(NPMBFormConstants.EDUCATION));		
		kinAddress.setValue(result.get(NPMBFormConstants.KIN_ADDRESS));
		kinName.setValue(result.get(NPMBFormConstants.NEXT_OF_KIN));
		kinRelationship.setValue(result.get(NPMBFormConstants.RELATIONSHIP));
		kinAge.setValue(result.get(NPMBFormConstants.KIN_AGE));		
		totalAnnualPay.setValue(result.get(NPMBFormConstants.TOTAL_ANNUAL_PAY));
		monthlyGrossPay.setValue(result.get(NPMBFormConstants.MONTHLY_GROSS_PAY));
		monthlyNetPay.setValue(result.get(NPMBFormConstants.MONTHLY_NET_PAY));
		totalFinancing.setValue(result.get(NPMBFormConstants.TOTAL_FINANCING));		
		totalAnnualPay.setValue(result.get(NPMBFormConstants.TOTAL_ANNUAL_PAY));
		monthlyGrossPay.setValue(result.get(NPMBFormConstants.MONTHLY_GROSS_PAY));
		monthlyNetPay.setValue(result.get(NPMBFormConstants.MONTHLY_NET_PAY));
		totalFinancing.setValue(result.get(NPMBFormConstants.TOTAL_FINANCING));		
		monthlyExpenses.setValue(result.get(NPMBFormConstants.MONTHLY_EXPENSES));
		equityContribution.setValue(result.get(NPMBFormConstants.EQUITY_CONTRIBUTION));
		tenor.setValue(result.get(NPMBFormConstants.TENOR));
		frequency.setValue(result.get(NPMBFormConstants.FREQUENCY));				
		loanAmount.setValue(result.get(NPMBFormConstants.LOAN_AMOUNT));
		maritalStatus.setValue(result.get(NPMBFormConstants.MARITAL_STATUS));
		spouseProfession.setValue(result.get(NPMBFormConstants.SPOUSE_PROFESSION));
		spouseEmployer.setValue(result.get(NPMBFormConstants.SPOUSE_EMPLOYER));
		spouseYears.setValue(result.get(NPMBFormConstants.SPOUSE_YEARS));
		employerNo.setValue(result.get(NPMBFormConstants.EMPLOYER_TEL));
		priorEmployer.setValue(result.get(NPMBFormConstants.EMPLOYER_PRIOR_NAME));
		priorYears.setValue(result.get(NPMBFormConstants.EMPLOYER_PRIOR_YEARS));
		priorAddress.setValue(result.get(NPMBFormConstants.EMPLOYER_PRIOR_ADDERSS));
		FormUtils.setOtherListCombo(guaranteeType, otherGuarantee, result.get(NPMBFormConstants.GUARANTEE_TYPE));
		setDeclarationAuthor(result);
	}

	public void enableEditing(boolean isEdit)
	{
		boolean enabled = isEdit;
		surname.setEnabled(enabled);
		firstName.setEnabled(enabled);
		middleName.setEnabled(enabled);
		employerAddress.setEnabled(enabled);
		residentialAddress.setEnabled(enabled);
		spouseAddress.setEnabled(enabled);
		mailingAddress.setEnabled(enabled);
		phone.setEnabled(enabled);		
		email.setEnabled(false);
		dob.setEnabled(enabled);
		pob.setEnabled(enabled);
		gender.setEnabled(enabled);
		nationality.setEnabled(enabled);
		origin.setEnabled(enabled);
		profession.setEnabled(enabled);
		employer.setEnabled(enabled);		
		position.setEnabled(enabled);
		employmentYears.setEnabled(enabled);
		employerEmail.setEnabled(enabled);
		retirementYears.setEnabled(enabled);				
		employerFax.setEnabled(enabled);
		residenceType.setEnabled(enabled); 
		residenceOther.setEnabled(enabled);
		rent.setEnabled(enabled);
		spouseName.setEnabled(enabled);				
		spouseIncome.setEnabled(enabled);
		spouseEmail.setEnabled(enabled);
		spousePhone.setEnabled(enabled);
		education.setEnabled(enabled);		
		kinAddress.setEnabled(enabled);
		kinName.setEnabled(enabled);
		kinRelationship.setEnabled(enabled);
		kinAge.setEnabled(enabled);		
		totalAnnualPay.setEnabled(enabled);
		monthlyGrossPay.setEnabled(enabled);
		monthlyNetPay.setEnabled(enabled);
		totalFinancing.setEnabled(enabled);		
		totalAnnualPay.setEnabled(enabled);
		monthlyGrossPay.setEnabled(enabled);
		monthlyNetPay.setEnabled(enabled);
		totalFinancing.setEnabled(enabled);		
		monthlyExpenses.setEnabled(enabled);
		equityContribution.setEnabled(enabled);
		tenor.setEnabled(enabled);
		frequency.setEnabled(enabled);				
		loanAmount.setEnabled(false);
		guaranteeType.setEnabled(enabled); 
		otherGuarantee.setEnabled(enabled); 
		dependentSlot.setEnabled(enabled);
		otherPeriodic.setEnabled(enabled);
		otherIncome.setEnabled(enabled);
		assets.setEnabled(enabled);
		obligations.setEnabled(enabled);
		banking.setEnabled(enabled);
		security.setEnabled(enabled);
		maritalStatus.setEnabled(enabled);
		spouseEmployer.setEnabled(enabled);
		spouseProfession.setEnabled(enabled);
		spouseYears.setEnabled(enabled);
		employerNo.setEnabled(enabled);
		priorEmployer.setEnabled(enabled);
		priorAddress.setEnabled(enabled);
		priorYears.setEnabled(enabled);
		alternateNo.setEnabled(enabled);
		enableButtons(enabled);
	}	
	
	public void setDeclarationAuthor(HashMap<String, String> result)
	{
		String[] nameParts = {result.get(NPMBFormConstants.SURNAME), result.get(NPMBFormConstants.FIRST_NAME),
				result.get(NPMBFormConstants.MIDDLE_NAME)};
		declarationAuthor.setText(FormUtils.getFullName(nameParts));		
	}
	
	public void clear()
	{
		setFormData(new HashMap<String, String>());
		dependentSlot.clear();
		otherPeriodic.clear();
		otherIncome.clear();
		banking.clear();
		assets.clear();
		security.clear(); 
		obligations.clear();
		enableButtons(false);
	}
	
	public void setLoanID(String loanID) 
	{
		clear();
		CustomerAppHelper.LOAN_MKT_SERVICE.getLoanApplicationWithLoanID(loanID, fetchFormDataCallback);
	}
}
