package com.fertiletech.mortgages.apply.forms;

import java.util.HashMap;
import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.fertiletech.mortgages.apply.CustomerAppHelper;
import com.fertiletech.mortgages.apply.NameTokens;
import com.fertiletech.mortgages.apply.utils.table.TablesView;
import com.fertiletech.mortgages.client.GUIConstants;
import com.fertiletech.mortgages.shared.DTOConstants;
import com.fertiletech.mortgages.shared.NPMBFormConstants;
import com.fertiletech.mortgages.shared.TableMessage;
import com.fertiletech.mortgages.shared.oauth.ClientUtils;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MortgageStatus extends Composite {

	@UiField
	TablesView statusTable;
	@UiField
	Paragraph info;
	@UiField
	Icon infoIndicator;
	@UiField
	Button newApp;

	private AsyncCallback<List<TableMessage>> statusCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper
					.showErrorMessage("Failed to load history, try refreshing your browser");
			setMessage(IconType.WARNING, false,
					"Failed to load, try a browser refresh. ERROR MESSAGE: "
							+ caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			newApp.setEnabled(Boolean.valueOf(result.get(0).getMessageId()));
			boolean init = statusTable.isInitialized();
			statusTable.showTable(result);
			if (!init) {
				statusTable.addColumn(getColumn(NameTokens.APPLY, "view"),
						"View");
				statusTable.addColumn(getColumn(NameTokens.PRINT, "print"),
						"Print");
			}
			setMessage(IconType.COPY, false, "Found " + result.size()
					+ " mortgage application(s)");
		}
	};
	
	protected AsyncCallback<String[]> newAppCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper.showErrorMessage("Unable to create a new application.<br/>" + caught.getMessage());
			newApp.setEnabled(true);
		}

		@Override
		public void onSuccess(String[] result) {
			String id = result[DTOConstants.LOAN_IDX];
			CustomerAppHelper.showInfoMessage("Application " + result[DTOConstants.LOAN_ID_IDX] + " created");
			CustomerAppHelper.LOAN_MKT_SERVICE.getSalesLeadCollection(id, statusCallback);
		}
	};

	private Column<TableMessage, SafeHtml> getColumn(final String token,
			final String display) {
		SafeHtmlCell cell = new SafeHtmlCell();
		Column<TableMessage, SafeHtml> urlColumn = new Column<TableMessage, SafeHtml>(
				cell) {

			@Override
			public SafeHtml getValue(TableMessage object) {
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				String url = "#" + token + "/" + object.getMessageId();
				sb.appendHtmlConstant("<a target='_blank' href='" + url + "'>"
						+ display + "</a>");
				return sb.toSafeHtml();
			}
		};
		return urlColumn;
	}

	private static MortgageStatusUiBinder uiBinder = GWT
			.create(MortgageStatusUiBinder.class);

	interface MortgageStatusUiBinder extends UiBinder<Widget, MortgageStatus> {
	}

	public MortgageStatus() {
		initWidget(uiBinder.createAndBindUi(this));
		newApp.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String email = ClientUtils.NPMBUserCookie.getCookie().getEmail();
				HashMap<String,String> appData = new HashMap<String, String>();
				appData.put(NPMBFormConstants.EMAIL, email);
				CustomerAppHelper.LOAN_MKT_SERVICE.startLoanApplication(appData, newAppCallback);

			}
		});
	}

	public void setLoandID(String id, boolean isOps, boolean isLoggedIn) {
		newApp.setEnabled(false);
		statusTable.clear();
		if (isLoggedIn) {
			if (id == null) 
			{
				if (isOps)
				{
					setMessage(
							IconType.COMPASS,
							false,
							"Looks like you did not access this page via M.A.P. <a href='"
									+ GUIConstants.MAP_URL_SELECT
									+ "'>Click here to select an application on M.A.P.</a>");
				}
				else
				{
					CustomerAppHelper
							.showInfoMessage("You have not yet applied for a mortgage with us. Redirecting to welcome page where you can start an application");
					History.newItem(NameTokens.WELCOME);
				}
			} 
			else 
			{
				setMessage(IconType.GEAR, true,
						"Loading history, please wait ...");
				CustomerAppHelper.LOAN_MKT_SERVICE.getSalesLeadCollection(id,
						statusCallback);
			}
		}
		else
		{
			setMessage(
					IconType.SIGN_IN,
					false,
					"You must be logged in before you can view your application history. "
							+ "Please sign in using the same email " +
							"address you specified on your application form");
		}
	}

	private void setMessage(IconType type, boolean spinIcon, String message) {
		infoIndicator.setType(type);
		infoIndicator.setSpin(spinIcon);
		info.setHTML(message);
	}
}
