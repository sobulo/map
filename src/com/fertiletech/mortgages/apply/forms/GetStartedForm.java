package com.fertiletech.mortgages.apply.forms;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;

import com.fertiletech.mortgages.apply.CustomerAppHelper;
import com.fertiletech.mortgages.apply.NameTokens;
import com.fertiletech.mortgages.apply.formwidgets.BSLoanFieldValidator;
import com.fertiletech.mortgages.apply.formwidgets.BSTextWidget;
import com.fertiletech.mortgages.shared.DuplicateEntitiesException;
import com.fertiletech.mortgages.shared.FormValidator;
import com.fertiletech.mortgages.shared.NPMBFormConstants;
import com.fertiletech.mortgages.shared.oauth.ClientUtils.NPMBUserCookie;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class GetStartedForm extends Composite{
	@UiField
	BSTextWidget<String> name;
	@UiField
	BSTextWidget<String> email;
	@UiField
	BSTextWidget<String> phone;
	@UiField
	BSTextWidget<String> income;
	@UiField
	Button apply;
	
	private static GetStartedFormUiBinder uiBinder = GWT
			.create(GetStartedFormUiBinder.class);

	interface GetStartedFormUiBinder extends UiBinder<Widget, GetStartedForm> {
	}

	protected AsyncCallback<String[]> applynowCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
			String msg = "Error processing application. ";
			if(caught instanceof DuplicateEntitiesException)
				msg = "Looks like you already applied. Try logging in to continue your application or contact customer service for assistance. ";
			else
			{
				apply.setEnabled(true);
				enableMiniApplyForm(true);
			}
			CustomerAppHelper.showErrorMessage(msg + caught.getMessage());
			apply.setEnabled(true);
			loanID = null;
			
		}

		@Override
		public void onSuccess(String[] result) {
			CustomerAppHelper.showInfoMessage("An application file has been created for you.");
			loanID = result[0];
			CustomerAppHelper.updateLatLngVals(loanID);
			History.newItem(NameTokens.APPLY + "/" + loanID); //go to loan form
		}
	};		
	
	public GetStartedForm() {
		initWidget(uiBinder.createAndBindUi(this));
		apply.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				BSTextWidget<String>[] fields = new BSTextWidget[4];
				fields[0] = email;
				fields[1] = phone;
				fields[2] = income;
				fields[3] = name;
				String[] formFields = {NPMBFormConstants.EMAIL, NPMBFormConstants.TEL_NO, NPMBFormConstants.TOTAL_ANNUAL_PAY, NPMBFormConstants.SURNAME};
				
				
				
				HashMap<String, String> errorMap = new HashMap<String, String>();
				for(int i = 0; i < formFields.length; i++)
				{
					String formKey = formFields[i];
					FormValidator[] oldStyleValidator = NPMBFormConstants.WELCOME_VALIDATORS.get(formKey);
					BSLoanFieldValidator validator = new BSLoanFieldValidator(oldStyleValidator, fields[i], formKey, errorMap, true);
					validator.markErrors();
				}
				
				if(errorMap.size() > 0)
					CustomerAppHelper.showErrorMessage(errorMap.size() + (errorMap.size()==1? " error" : " errors") + " found. See highlighted fields.");
				else
				{
					if(loanID != null)
					{
						
						History.newItem(NameTokens.APPLY + "/" + loanID);
						return;
					}
					enableMiniApplyForm(false);
					apply.setEnabled(false);
					CustomerAppHelper.LOAN_MKT_SERVICE.startLoanApplication( getFormData(), applynowCallback);
				}
			}			
		});		
		
	}

	String loanID;
	private AsyncCallback<HashMap<String, String>> loadCallback = new AsyncCallback<HashMap<String,String>>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper.showErrorMessage("Error reaching server. Try refreshing your broweser." + caught.getMessage());
			apply.setEnabled(true);
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			if(email.getValue().trim().length() > 0)
			{
				if(!email.getValue().trim().equalsIgnoreCase(result.get(NPMBFormConstants.EMAIL)))
				{
					History.newItem(NameTokens.WELCOME); //blank out param arg in url
					return;
				}
			}
			setFormData(result);
			apply.setText("Continue Application");
			loanID = result.get(NPMBFormConstants.ID);
			apply.setEnabled(true);
		}
	};
	
	public void enableMiniApplyForm(boolean enabled)
	{
		email.setEnabled(enabled);
		phone.setEnabled(enabled);
		income.setEnabled(enabled);
		name.setEnabled(enabled);
		
	}
	
	public void setAndLockEmail(String address)
	{
		email.setValue(address);
		email.setEnabled(false);
	}
	
	public void setName(String myName)
	{
		name.setValue(myName);
	}
	
	public void clear()
	{
		setFormData(new HashMap<String, String>());
		apply.setText("Get Started");
	}
	
	public void setFormData(HashMap<String, String> data)
	{
		email.setValue(data.get(NPMBFormConstants.EMAIL));
		phone.setValue(data.get(NPMBFormConstants.TEL_NO));
		income.setValue(data.get(NPMBFormConstants.TOTAL_ANNUAL_PAY));
		String[] nameParts = {data.get(NPMBFormConstants.SURNAME), data.get(NPMBFormConstants.FIRST_NAME),
				data.get(NPMBFormConstants.MIDDLE_NAME)}; 
		name.setValue(FormUtils.getFullName(nameParts));
	}
	
	public HashMap<String, String> getFormData()
	{
		HashMap<String, String> result = new HashMap<String, String>();
		result.put(NPMBFormConstants.EMAIL, email.getValue());
		result.put(NPMBFormConstants.TEL_NO, phone.getValue());
		result.put(NPMBFormConstants.TOTAL_ANNUAL_PAY, income.getValue());
		String nameParts[] = getNameParts(name.getValue());
		result.put(NPMBFormConstants.SURNAME, nameParts[0]);
		if(nameParts[1] != null)
			result.put(NPMBFormConstants.FIRST_NAME, nameParts[1]);
		if(nameParts[2] != null)
			result.put(NPMBFormConstants.MIDDLE_NAME, nameParts[2]);
		return result;
	}
	

	
	public String[] getNameParts(String fullName)
	{
		String[] parts = fullName.split(" ");
		for(int i = 0; i < parts.length; i++)
			parts[i] = parts[i].trim();
		String[] result = new String[3];
		if(parts.length == 1)
			result[0] = parts[0];
		else if( parts.length == 2)
		{
			result[0] = parts[1];
			result[1] = parts[0];
		}
		else if(parts.length > 2)
		{
			result[0] = parts[2];
			result[1] = parts[0];
			result[2] = parts[1];
		}
		return result;
	}
	
	public void enableApplyButton(boolean enabled)
	{
		apply.setEnabled(enabled);
	}
	
	public void updateForm(String[] args, boolean isOps, boolean isLoggedIn)
	{
		String id = CustomerAppHelper.getLoanID(args);
		clear();
		
		if(id != null)
		{
			apply.setEnabled(false);
			enableMiniApplyForm(false);
			CustomerAppHelper.LOAN_MKT_SERVICE.getLoanApplicationWithLoanID(id, loadCallback);
		}
		
		if(isLoggedIn && !isOps)
		{
			NPMBUserCookie uc = NPMBUserCookie.getCookie();
			setAndLockEmail(uc.getEmail());
			name.setValue(uc.getUserName());
		}
	}

}
