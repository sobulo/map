package com.fertiletech.mortgages.office.content;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.mortgages.client.MyAsyncCallback;
import com.fertiletech.mortgages.office.PanelUtilities;
import com.fertiletech.mortgages.shared.DTOConstants;
import com.fertiletech.mortgages.shared.TableMessage;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.map.Map;
import com.googlecode.gwt.charts.client.map.MapOptions;
import com.googlecode.gwt.charts.client.options.MapType;
import com.googlecode.gwt.charts.client.options.Options;

public class MapChartPanel extends MortgageChartPanel{
	protected ChartPackage getChartType()
	{
		return ChartPackage.MAP;
	}	
	protected ChartWidget<? extends Options> getChart() {
		final Map map = new Map();
		MapOptions options = MapOptions.create();
		options.setShowTip(true);
		options.setMapType(MapType.NORMAL);
		options.setUseMapTypeControl(true);	
		opt = options;	
		return map;
	}

	protected void drawChart(final Date startDate, final Date endDate) {
		final Map mapChart = (Map) chart;
		final ListBox categoryTypes = new ListBox();
		footer.clear();
		HorizontalPanel catPanel = new HorizontalPanel();
		catPanel.setSpacing(10);
		catPanel.add(new Label("Type: "));
		catPanel.add(categoryTypes);
		footer.add(catPanel);
		container.setWidgetSize(footer, 50);
		container.animate(1000);
		MyAsyncCallback<List<TableMessage>> mapCallBack = new MyAsyncCallback<List<TableMessage>>() {

			private String getSelectedCategory()
			{
				return categoryTypes.getValue(categoryTypes.getSelectedIndex());
			}
			private void drawMap(List<TableMessage> catList)
			{
				mapChart.setTitle("Displaying " + catList.size() + " entries for [" + getSelectedCategory()+"]");
				mapChart.draw(getTable(catList), (MapOptions) opt);										
			}
			@Override
			public void onSuccess(List<TableMessage> result) {
				// Prepare the data
				if(result.size() == 0)
				{
					PanelUtilities.errorBox.show("No results found. Try widening specified date range");
					return;
				}			

				final HashMap<String, List<TableMessage>> categoryMap = categorizeData(result);
				categoryTypes.clear();
				int largestCategory = 0;
				int count = 0;
				int initIdx = 0;
				for(String type : categoryMap.keySet())
				{
					if(categoryMap.get(type).size() > largestCategory)
					{
						largestCategory = categoryMap.get(type).size();
						initIdx = count;
					}
					categoryTypes.addItem(type);
					count++;
				}
				categoryTypes.addChangeHandler(new ChangeHandler() {
					
					@Override
					public void onChange(ChangeEvent event) {
						drawMap(categoryMap.get(getSelectedCategory()));
					}
				});
				categoryTypes.setSelectedIndex(initIdx);
				drawMap(categoryMap.get(getSelectedCategory()));				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Error occured on NPMB cloud. Details: " + caught.getMessage());
			}
			
			@Override
			protected void callService(AsyncCallback<List<TableMessage>> cb) {
				PanelUtilities.getLoginService().getMapAttachments(startDate, endDate, cb);		
			}
		};
		mapCallBack.go("Fetching map data, please wait ...");
	}	
	private HashMap<String, List<TableMessage>> categorizeData(List<TableMessage> locations)
	{
		HashMap<String, List<TableMessage>> result = new HashMap<String, List<TableMessage>>();
		for(TableMessage loc : locations)
		{
			String cat = loc.getText(DTOConstants.CATEGORY_IDX);
			List<TableMessage> catLocations = result.get(cat);
			if(catLocations == null)
				catLocations = new ArrayList<TableMessage>();
			catLocations.add(loc);
			result.put(cat, catLocations);
		}
		return result;
	}
	
	private DataTable getTable(List<TableMessage> result)
	{
		DataTable dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.NUMBER, "A");
		dataTable.addColumn(ColumnType.NUMBER, "B");
		dataTable.addColumn(ColumnType.STRING, "Home");
		dataTable.addRows(result.size());
		for(int i = 0; i < result.size(); i++)
		{
			String detail = result.get(i).getText(DTOConstants.DETAIL_IDX);
			dataTable.setValue(i, 0, result.get(i).getNumber(DTOConstants.LAT_IDX));
			dataTable.setValue(i, 1, result.get(i).getNumber(DTOConstants.LNG_IDX));
			dataTable.setValue(i, 2, detail);
		}
		return dataTable;
	}
}
