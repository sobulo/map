/**
 * 
 */
package com.fertiletech.mortgages.office.content;



import com.fertiletech.mortgages.office.PanelUtilities;
import com.fertiletech.mortgages.office.util.YesNoDialog;
import com.fertiletech.mortgages.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ImageUploadPanel extends Composite implements ClickHandler, FormPanel.SubmitCompleteHandler{

	@UiField Button submit;
	@UiField HTML status;
	@UiField FileUpload selectFile;
	@UiField FormPanel imageForm;
	@UiField ListBox purpose;
	
	private static ImageUploadUiBinder uiBinder = GWT
			.create(ImageUploadUiBinder.class);

	interface ImageUploadUiBinder extends UiBinder<Widget, ImageUploadPanel> {
	}

	public ImageUploadPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		purpose.addItem("NPMB Mortgage Form Header", DTOConstants.NPMB_FORM_HEADER);
		//purpose.addItem("NPMB NHIS Form Logo", DTOConstants.NPMB_FORM_HEADER);
        submit.addClickHandler(this);
        imageForm.setAction("/upload/images");
        imageForm.addSubmitCompleteHandler(this);
        imageForm.setMethod(FormPanel.METHOD_POST);
        imageForm.setEncoding(FormPanel.ENCODING_MULTIPART);
        selectFile.setName("chooseFile");
        PanelUtilities.getLoginService().getImageUploadHistory(DTOConstants.NPMB_FORM_HEADER, new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				status.setHTML(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Error uploading image. Details: " + caught.getMessage());
				status.setHTML("<font color='red'> ERROR UPLOADING</font><br/>" + caught.getMessage());
			}
		});
	}
	
    @Override
    public void onSubmitComplete(FormPanel.SubmitCompleteEvent event)
    {
        String results = event.getResults();
        status.setHTML(results);
        GWT.log(results);
    }

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		final YesNoDialog confirmUpload = new YesNoDialog("WARNING - Reserved for IT");
		confirmUpload.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				confirmUpload.hide();
				status.setText("Sending file to server, please wait...");
				imageForm.submit();
			}
		});
		confirmUpload.show("<p>Uploading a logo could break pdf downloads. Please ensure you" +
				" test pdf downloads of mortgage applications before and AFTER using this module.</p>. See help page for more information");
	}	

}
