package com.fertiletech.mortgages.office.content.shell;


import com.fertiletech.mortgages.office.ContentWidget;
import com.fertiletech.mortgages.office.HelpPageGenerator;
import com.fertiletech.mortgages.office.content.ImageUploadPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class PDFReportImagesCW extends ContentWidget{

	public PDFReportImagesCW() {
		super("PDF Logo", "Company Logo used in PDF reports generated by M.A.P.");
	}

	@Override
	public Widget onInitialize() {
		return new ImageUploadPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(PDFReportImagesCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ADMIN_URL;
	}

}
