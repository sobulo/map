package com.fertiletech.mortgages.office.content.shell;

import com.fertiletech.mortgages.office.PanelUtilities;
import com.fertiletech.mortgages.office.util.SimpleDialog;
import com.fertiletech.mortgages.office.util.text.RichTextToolbar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class CommentsPanel extends Composite {

	Button save;
	@UiField SimplePanel commentsSlot;
	@UiField HTML priorComments;
	protected RichTextArea richText;
	SimpleDialog infoBox;
	private final int MIN_COMMENT_LENGTH = 30;
	protected String commentID;
	RichTextToolbar tb;
	
	private static CommentsPanelUiBinder uiBinder = GWT
			.create(CommentsPanelUiBinder.class);

	interface CommentsPanelUiBinder extends UiBinder<Widget, CommentsPanel> {
	}

	protected final AsyncCallback<String> loadCallback = new AsyncCallback<String>() {

	      @Override
	      public void onSuccess(String result) {
	    	  enableSaveWidgets(true);
	    	  priorComments.setHTML(result);
	      }

	      @Override
	      public void onFailure(Throwable caught) {
	      	infoBox.show("Try refreshing your brower. Error loading comments. " + caught.getMessage());
	      }
	  };

	protected final AsyncCallback<Void> saveCallback = new AsyncCallback<Void>() {

		      @Override
		      public void onSuccess(Void result) {
		    	  loadComments();
		    	  enableSaveWidgets(true);
		    	  infoBox.show("Saved Succesfully");
		    	  clear(false);
		      }

		      @Override
		      public void onFailure(Throwable caught) {
		      	infoBox.show("Error loading: " + caught.getMessage());
		      	enableSaveWidgets(true);
		      }
		  };	  
		  
	public CommentsPanel(boolean includeSaveWidgets) {
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		if(includeSaveWidgets)
			addSaveWidgets();
	}
	
	public CommentsPanel()
	{
		this(true);
	}
	
	protected void addSaveWidgets()
	{
		save = new Button("Save Comments");
		commentsSlot.add(getRichTextArea());
		save.addClickHandler(getSaveHandler());
		enableSaveWidgets(false);
	}
	
	private void enableSaveWidgets(boolean enabled)
	{
		if(save != null)
		{
			richText.setEnabled(enabled);
			save.setEnabled(enabled);
			tb.setEnabled(enabled);
		}

	}

	private ClickHandler getSaveHandler()
	{
		return new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(richText.getText().length() < MIN_COMMENT_LENGTH)
				{
					infoBox.show("Comment length too short. Please enter a more detailed description");
					return;
				}
				else if(commentID == null)
				{
					infoBox.show("No object associated with this comment box, " +
							"please contact technology@fertiletech.com if problems persist");
					return;
				}
				saveComments();
			}
		};
	}

	private Widget getRichTextArea()
	{
		HorizontalPanel p = new HorizontalPanel();
		richText = new RichTextArea();
	    tb = new RichTextToolbar(richText, save);
	    p.add(richText);
	    p.add(tb);
	    richText.setHeight("100px");
	    richText.setWidth("100%");
	    //tb.setWidth("100%");
	    p.setSpacing(5);
	    p.setWidth("100%");	
	    return p;	
	}	
		
	public void setCommentID(String id)
	{
		commentID = id;
		loadComments();
		GWT.log("Comment ID after set is: " + commentID + " value passed in is: " + id);
	}
		
	public void clear()
	{
		clear(true);
	}
	
	protected void loadComments()
	{
  	  	priorComments.setHTML("<marquee>Loading comments ...</marquee>");
		PanelUtilities.getLoginService().loadActivityComments(commentID, loadCallback);		
	}
	
	protected void saveComments()
	{
		enableSaveWidgets(false);
		PanelUtilities.getLoginService().saveActivityComment(commentID, richText.getHTML(), false, saveCallback);		
	}
	
	protected void clear(boolean clearPrior)
	{
		if(clearPrior)
		{
			enableSaveWidgets(false);
			priorComments.setHTML("");
			commentID = null;
		}
		richText.setText("");
	}
}