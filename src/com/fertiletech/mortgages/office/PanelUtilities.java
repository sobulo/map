/**
 * 
 */
package com.fertiletech.mortgages.office;

import com.fertiletech.mortgages.client.LoginService;
import com.fertiletech.mortgages.client.LoginServiceAsync;
import com.fertiletech.mortgages.client.MortgageService;
import com.fertiletech.mortgages.client.MortgageServiceAsync;
import com.fertiletech.mortgages.office.util.SimpleDialog;
import com.google.gwt.core.client.GWT;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PanelUtilities {
	public final static SimpleDialog infoBox = new SimpleDialog("<center><b style='color:green'>INFO</b></center>", true);
	public final static SimpleDialog errorBox = new SimpleDialog("<center><b style='color:red'>Error!</b></center>", true);

    final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
    final static MortgageServiceAsync LOAN_MKT_SERVICE = GWT.create(MortgageService.class);

    public static MortgageServiceAsync getLoanMktService()
    {
    	return LOAN_MKT_SERVICE;
    }
    
    public static LoginServiceAsync getLoginService()
    {
    	return READ_SERVICE;
    }	
}
