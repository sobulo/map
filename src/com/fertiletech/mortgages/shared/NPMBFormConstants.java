package com.fertiletech.mortgages.shared;

import java.util.HashMap;

import com.fertiletech.mortgages.shared.TableMessageHeader.TableMessageContent;


public class NPMBFormConstants {
	
	//section A fields
	public static String ID = "NPMB-Portal-ID";
	public static String ID_NUM = "NPMB-Portal-NO";
	public static String ID_STATE = "NPMB-State";
	public static String ID_EDIT = "NPMB-Write";
	public static String SURNAME = "Surname";
	public static String FIRST_NAME = "First Name";
	public static String MIDDLE_NAME = "Middle Name";
	public static String ADDRESS = "Address";
	public static String TEL_NO = "Tel No";
	public static String MOBILE_NO = "Mobile No";
	public static String EMAIL = "Personal Email";
	public static String DATE_OF_BIRTH = "Date of Birth";
	public static String PLACE_OF_BIRTH = "Place of Birth";
	public static String GENDER = "Sex";
	public static String ORIGIN_STATE = "State of Origin";
	public static String NATIONALITY = "Nationality";
	public static String EDUCATION = "Educational Qualification";
	public static String PROFESSION = "Profession";
	public static String EMPLOYER = "Employer Name";
	public static String EMPLOYER_ADDRESS = "Employer Address";
	public static String EMPLOYER_WEBSITE = "Employer Website";
	public static String EMPLOYER_TEL = "Employer Phone";
	public static String POSITION = "Present Position";
	public static String FAX = "Fax";
	public static String RESIDENTIAL_ADDRESS = "Residential Address";
	public static String RESIDENTIAL_TEL_NO = "Residential Tel No";
	public static String OFFICE_NO = "Office Tel No";
	public static String OFFICE_EMAIL = "Office Email";
	public static String EMPLOYER_YEARS = "Years of Current Employment";
	public static String RETIREMENT = "Years to Retirement";
	public static String RESIDENTIAL_TYPE = "Residence Type";
	public static String ANNUAL_RENT = "Annual Rent";
	public static String MARITAL_STATUS = "Marital Status";
	public static String SPOUSE_NAME = "Name of Spouse";
	public static String SPOUSE_ADDRESS = "Spouse's Address";
	public static String SPOUSE_EMPLOYER = "Spouse's Employer";
	public static String SPOUSE_INCOME = "Spouse's Annual Income";
	public static String SPOUSE_EMAIL = "Spouse's Email";
	public static String SPOUSE_PHONE = "Spouse's Phone";	
	public static String NO_OF_CHILDREN = "Children/Other Dependents";
	public static String NEXT_OF_KIN = "Kin Name"; //TODO map
	public static String RELATIONSHIP = "Kin Relationship";
	public static String KIN_ADDRESS = "Kin Address";
	public static String KIN_AGE = "Kin Age";
	public static String TOTAL_ANNUAL_PAY = "Total Annual Pay";
	public static String MONTHLY_GROSS_PAY = "Monthly Gross Pay";
	public static String MONTHLY_NET_PAY = "Monthly Net Pay";
	public static String MONTHLY_EXPENSES = "Monthly Expenses";
	public static String TOTAL_FINANCING = "Total Financing";
	public static String EQUITY_CONTRIBUTION = "Equity Contribution";
	public static String TENOR = "Tenor";
	public static String FREQUENCY = "Frequency";
	public static String GUARANTEE_TYPE = "Guarantee Type";
	public static String LOAN_AMOUNT = "Loan Amount";
	public final static String EMPLOYER_PRIOR_NAME = "Prior Employer's Name";
	public final static String EMPLOYER_PRIOR_ADDERSS = "Prior Employer Address";
	public final static String EMPLOYER_PRIOR_YEARS = "Prior Employer Years";
	public final static String SPOUSE_YEARS = "Spouse's Work Years";
	public final static String SPOUSE_PROFESSION = "Spouse's Profession";
	
	
	
	//declaration/signature by user upon completion
	public static String DECLARATION = "I hereby declare that all information provided in this application are correct and" +
			" that all documents submitted with this form are authentic. I agree that New Prudential could cross-check the information " +
			"provided above and may request for references from my employer and/or financial institutions mentioned above. " +
			"I also agree that New Prudential may obtain and disclose credit reference or information on me from or to credit bureaus. " +
			"I therefore agree that any material misstatement discovered renders my application null and void.";
	
	public static int DEPENDENT_IDX = 0;
	public static int OTHER_PERIODIC_IDX = 1;
	public static int OTHER_INCOME_IDX = 2;
	public static int ASSET_IDX = 3;
	public static int OBLIGATION_IDX = 4;
	public static int BANKING_IDX = 5;
	public static int SECURITY_IDX = 6;
	
	public static String[] TABLE_DESCRIPTIONS = {"Dependents", "Other Periodic Income", "Other Income", "Assets", 
												 "Obligations", "Banking", "Security"};
	
	
	public final static String[] GENDER_LIST = {"Male", "Female"};
	public final static String[] RESIDENCE_TYPES_LIST = {"Owned", "Rented", "Other"};
	public final static String[] EDUCATION_LIST = {"O-Levels", "National Diploma", "Bachelors", "Masters", "Doctorate"};
	public final static String[] MARITAL_STATUS_LIST = {"Married", "Single", "Divorced/Separated", "Widowed"};
	public final static String[] GUARANTEE_TYPE_LIST = {"Guarantee of Employer", "Other Guarantee"};
	public final static String[] FREQUENCY_LIST = {"Annually (1 payment/year)", "Bi-Annual (2 payments/year)", "Quaterly (4 payments/year)", "Bi-Monthly (6 payments/year)", "Monthly (12 payments/year)","Semi-Monthly (24 payments/year)","Bi-weekly (26 payments/year)","Weekly (52 payments/year)"};
	//flex panel headers and widths
	public static String[][] DEPENDENT_TABLE_CFG = {{"Name", "Age", "Relationship"},{"50", "15", "35"}, {"dependent", "dependents"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString()}};
	public static String[][] PERIODIC_TABLE_CFG = {{"Type", "Amount", "Month"},{"40", "30", "30"},{"",""}, {TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString(), TableMessageContent.TEXT.toString()}};
	public static String[][] INCOME_SRC_TABLE_CFG = {{"Source", "Amount/Year"},{"60", "40"}, {"source", "sources"}, {TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][] ASSET_TABLE_CFG = {{"Property/Investment", "Estimated Value", "Yearly Income"},{"40", "30", "30"}, {"asset", "assets"}, {TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][] OBLIGATION_TABLE_CFG = {{"Lender", "Type", "Outstanding Amount", "Existing Periodic Repayment", "Frequency"},{"25", "15", "20", "25", "15"}, {"obligation", "obligations"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString(), TableMessageContent.NUMBER.toString(), TableMessageContent.TEXT.toString()}};
	public static String[][] BANKING_TABLE_CFG = {{"Bank", "Account No.", "Account Type", "Balance"},{"30", "30", "15", "25"}, {"bank account", "bank accounts"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][] SECURITY_TABLE_CFG = {{"Asset Type", "Type of Charge Proposed", "Valuation"},{"35", "35", "30"}, {"loan security", "loan securities"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString()}};
	public static String[][][] ALL_CONFIG = {DEPENDENT_TABLE_CFG, PERIODIC_TABLE_CFG, INCOME_SRC_TABLE_CFG, 
											ASSET_TABLE_CFG, OBLIGATION_TABLE_CFG, BANKING_TABLE_CFG, SECURITY_TABLE_CFG};
	
	//form validators for welcome page
	public static HashMap<String, FormValidator[]> WELCOME_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_PERSONAL_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_FAMILY_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_EMPLOYMENT_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_FINANCIAL_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_LOAN_VALIDATORS = new HashMap<String, FormValidator[]>();

	static
	{
    	FormValidator[] mandatoryOnly = {FormValidator.MANDATORY};
    	FormValidator[] emailOnly = {FormValidator.EMAIL};
    	FormValidator[] phoneOnly = {FormValidator.PHONE};
    	FormValidator[] mandatoryPlusPhone = {FormValidator.MANDATORY, FormValidator.PHONE};
    	FormValidator[] mandatoryPlusEmail = {FormValidator.MANDATORY, FormValidator.EMAIL};
    	FormValidator[] mandatoryPlusParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_INTEGER};
    	FormValidator[] mandatoryPlusDateParsesOk = {FormValidator.MANDATORY, FormValidator.DATE_OLD_ENOUGH};
    	FormValidator[] mandatoryPlusLoanParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_DOUBLE};		
    	
    	WELCOME_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	WELCOME_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	WELCOME_VALIDATORS.put(SURNAME, mandatoryOnly);
    	WELCOME_VALIDATORS.put(TOTAL_ANNUAL_PAY, mandatoryPlusLoanParsesOk);
    	
    	NPMB_PERSONAL_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	NPMB_PERSONAL_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	NPMB_PERSONAL_VALIDATORS.put(SURNAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(RESIDENTIAL_ADDRESS, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(DATE_OF_BIRTH, mandatoryPlusDateParsesOk);
    	NPMB_PERSONAL_VALIDATORS.put(MOBILE_NO, phoneOnly);
    	
    	NPMB_EMPLOYMENT_VALIDATORS.put(OFFICE_EMAIL, emailOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(OFFICE_NO, phoneOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(EMPLOYER, mandatoryOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(EMPLOYER_ADDRESS, mandatoryOnly);
    	NPMB_EMPLOYMENT_VALIDATORS.put(FAX, phoneOnly);
    	
    	NPMB_FAMILY_VALIDATORS.put(KIN_ADDRESS, mandatoryOnly);
    	NPMB_FAMILY_VALIDATORS.put(NEXT_OF_KIN, mandatoryOnly);
    	NPMB_FAMILY_VALIDATORS.put(SPOUSE_EMAIL, emailOnly);
    	NPMB_FAMILY_VALIDATORS.put(SPOUSE_PHONE, phoneOnly);
    	
    	NPMB_FINANCIAL_VALIDATORS.put(TOTAL_ANNUAL_PAY, mandatoryPlusLoanParsesOk);
    	NPMB_FINANCIAL_VALIDATORS.put(MONTHLY_NET_PAY, mandatoryPlusLoanParsesOk);
    	NPMB_FINANCIAL_VALIDATORS.put(MONTHLY_GROSS_PAY, mandatoryPlusLoanParsesOk);
    	NPMB_FINANCIAL_VALIDATORS.put(MONTHLY_EXPENSES, mandatoryPlusLoanParsesOk);
    	
    	NPMB_LOAN_VALIDATORS.put(TOTAL_FINANCING, mandatoryPlusLoanParsesOk);
    	NPMB_LOAN_VALIDATORS.put(EQUITY_CONTRIBUTION, mandatoryPlusLoanParsesOk);
    	NPMB_LOAN_VALIDATORS.put(LOAN_AMOUNT, mandatoryPlusLoanParsesOk);
	}
	
	public static Boolean[] getSubmissionValues(TableMessage result)
	{
		Boolean appSubmitted, guard1Submitted, guard2Submitted;
		
		long subVal = Math.round(result.getNumber(DTOConstants.LOAN_KEY_IDX));
		if( subVal == 1)
			appSubmitted = true;
		else 
			appSubmitted = false;
		
		Boolean[] subs = new Boolean[1];
		subs[0] = appSubmitted;
		return subs;
	}	
	

}
